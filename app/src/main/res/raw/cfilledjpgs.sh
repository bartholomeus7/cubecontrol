#!/bin/bash
convert -size 16x16 xc:white empty.jpg   
for var in "$@"
do
	convert empty.jpg -fill \#"$var" -draw 'color 0,0 reset' x"$var".jpg
	echo create one color texture with color "$var" 
done
rm empty.jpg
echo Done.

