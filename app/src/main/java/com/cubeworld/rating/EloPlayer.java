package com.cubeworld.rating;

import com.cubeworld.func.Observer;
import com.cubeworld.misc.GPersistable;
import com.google.gson.annotations.Expose;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public final class EloPlayer implements ObservableRatable {


    private int totalPlays = 0;
    private double elo = 1500;

    @Expose
    private transient List<Observer<Double>> observableList = new ArrayList<>();


    public enum PlayerType {

        RATED, TRAIN;


        private static TypeToken<EloPlayer> PLAYER_TYPE_TOKEN = new TypeToken<EloPlayer>() {
        };
        private static GPersistable<EloPlayer> RATED_PLAYER = new GPersistable<EloPlayer>("rplayer", PLAYER_TYPE_TOKEN, new EloPlayer()) {
            @Override
            public void set(EloPlayer player) {
                throw new IllegalStateException("Cannot assign player to immutable multiton");
            }
        };
        private static GPersistable<EloPlayer> TRAIN_PLAYER = new GPersistable<EloPlayer>("uplayer", PLAYER_TYPE_TOKEN, new EloPlayer()) {
            @Override
            public void set(EloPlayer player) {
                throw new IllegalStateException("Cannot assign player to immutable multiton");
            }
        };

        public GPersistable<EloPlayer> get() {
            switch (this) {
                case RATED:
                    return RATED_PLAYER;
                case TRAIN:
                    return TRAIN_PLAYER;
            }
            throw new IllegalStateException("Unreachable");
        }
    }

    @Override
    public double getRating() {
        return elo;
    }

    @Override
    public double adaptRating(double plus) {
        elo += plus;
        totalPlays++;
        for (Observer<Double> o : observableList) {
            o.update(this, elo);
        }
        return elo;
    }

    @Override
    public double getWeight() {
        if (totalPlays < 50)
            return 40;
        else if (totalPlays < 200)
            return 30;
        else
            return 20;
    }

    private EloPlayer() {

    }

    private EloPlayer(EloPlayer other) {
        this.elo = other.elo;
        this.totalPlays = other.totalPlays;
    }


    public EloPlayer clone() {
        return new EloPlayer(this);
    }


    @Override
    public void register(Observer<Double> observer) {
        observableList.add(observer);
    }

    @Override
    public boolean unregister(Observer<Double> observer) {
        return observableList.remove(observer);
    }
}
