package com.cubeworld.rating;

import com.cubeworld.func.Listener;
import com.cubeworld.func.Observable;
import com.cubeworld.func.Observer;
import com.cubeworld.json.JsonAsynchronPool;
import com.cubeworld.cubes.PlayActivity;
import com.cubeworld.misc.Dates;
import com.cubeworld.misc.GPersistable;
import com.cubeworld.misc.Persistable;
import com.cubeworld.misc.Settings;
import com.cubeworld.riddles.CubeRiddle;
import com.google.gson.reflect.TypeToken;
import com.threed.jpct.Logger;

import java.util.Date;
import java.util.LinkedHashMap;

/**
 * Created by fortknock on 06.02.16.
 */
public class StatisticManager extends Persistable implements Observer<Match> {

    private static final String statService = "http://cube-1179.appspot.com/playerstats";
    private static StatisticManager instance = null;

    private GPersistable<PlayerRun> player_run = new GPersistable<>("playerStatistics", new TypeToken<PlayerRun>() {
    }, getNewPlayerRun());
    private final GPersistable<Date> last_send = new GPersistable<>("last_send", new TypeToken<Date>() {
    }, new Date(0));

    private JsonAsynchronPool jpool = PlayActivity.getJSoonPool();

    private StatisticManager() {
        player_run.get();
    }

    public void update(Observable<Match> source, Match object) {
        PlayerRun currentRun = player_run.get();

        if (!(object.getPlayer1() instanceof EloPlayer) || !(object.getPlayer2() instanceof CubeRiddle)) {
            throw new IllegalArgumentException("First player must be player, second must be cuberiddle.");
        }
        if (currentRun.getMap().size() < Settings.get().getStatistic_max_cache()) {
            CubeRiddle cubeRiddle = (CubeRiddle) object.getPlayer2();
            currentRun.getMap().put(cubeRiddle.getId(), object.getResult());
            Logger.log("Current statistic cache size= " + currentRun.getMap().size());
        }


        if (shouldTryToSend(currentRun.getMap().size())) {
            Logger.log("TRYING TO SEND!!!");
            jpool.sendAsynchron(statService, currentRun, new Listener<Object, Throwable>() {
                @Override
                public void action(Object o, Throwable throwable) {
                    if (throwable == null) player_run.set(getNewPlayerRun());
                    else Logger.log(throwable.getMessage());
                }
            });
        }
    }

    private boolean shouldTryToSend(int mapSize) {
        return mapSize >= Settings.get().getStatistic_max_cache() && Dates.isXDaysPast(last_send.get(), Settings.get().getStatistics_send_interval());
    }

    public static StatisticManager getInstance() {
        if (instance == null) {
            instance = new StatisticManager();
        }
        return instance;
    }

    @Override
    public boolean persist() {
        return player_run.persist();
    }

    private PlayerRun getNewPlayerRun() {
        return new PlayerRun(EloPlayer.PlayerType.RATED.get().get().clone(), new LinkedHashMap<Long, MultipleChoiceResult>());
    }
}
