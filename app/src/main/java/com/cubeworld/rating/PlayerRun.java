package com.cubeworld.rating;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by fortknock on 10.01.16.
 */
public class PlayerRun {
    private EloPlayer Player;
    private Map<Long, MultipleChoiceResult> map = new LinkedHashMap<>();

    public PlayerRun(EloPlayer player, Map<Long, MultipleChoiceResult> map) {
        Player = player;
        this.map = map;
    }

    public EloPlayer getPlayer() {
        return Player;
    }

    public void setPlayer(EloPlayer player) {
        Player = player;
    }

    public Map<Long, MultipleChoiceResult> getMap() {
        return map;
    }

    public void setMap(Map<Long, MultipleChoiceResult> map) {
        this.map = map;
    }
}
