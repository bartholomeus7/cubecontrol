package com.cubeworld.rating;

public interface Ratable {
    double getRating();

    double adaptRating(double plus);

    double getWeight();
}
