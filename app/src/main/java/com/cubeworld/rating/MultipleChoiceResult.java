package com.cubeworld.rating;

public enum MultipleChoiceResult {
    RIGHT_ANSWER, WRONG_ANSWER, PASS
}
