package com.cubeworld.rating;

import com.cubeworld.func.Observable;
import com.cubeworld.func.Observer;

/**
 * This is an adapted version of elo rating specially designed for multiple
 * choice system with limited time. This rating system implements the idea that in this case
 * guessing is always possible and should not rewarded, which means its biasof point gain
 * should be equal to the bias of not answering at all. Therefore, the punishment
 * for a wrong answer is linear in the probability that a player actually has
 * known the right answer, which we model with elo system itself.
 *
 * @author Tobias Krüner
 */

public class MultipleChoiceElo implements Observer<Match> {

    public void adaptRatings(Ratable acteur, Ratable mp, MultipleChoiceResult mpResult) {
        switch (mpResult) {
            case RIGHT_ANSWER:
                adaptRatings(acteur, mp);
                break;
            case PASS:
                adaptRatings(mp, acteur);
                break;
            case WRONG_ANSWER:
                double chance = getChanceFirstPlayerWinning(acteur.getRating(), mp.getRating());
                double xfactor = -chance - 1 / 3d;
                acteur.adaptRating(acteur.getWeight() * xfactor);
                mp.adaptRating(mp.getWeight() * -xfactor);
                break;
        }
    }

    public static void adaptRatings(Ratable winner, Ratable loser) {
        double expectedWinner = 1d / (1 + Math.pow(10d, (loser.getRating() - winner.getRating()) / 400d));
        double expectedLoser = 1d - expectedWinner;
        winner.adaptRating(winner.getWeight() * (1d - expectedWinner));
        loser.adaptRating(-loser.getWeight() * expectedLoser);
    }

    public static double getChanceFirstPlayerWinning(double first, double second) {
        return 1d / (1d + Math.pow(10d, (second - first) / 400d));
    }


    @Override
    public void update(Observable<Match> source, Match object) {
        adaptRatings(object.getPlayer1(), object.getPlayer2(), object.getResult());
    }
}
