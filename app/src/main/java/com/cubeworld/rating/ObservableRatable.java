package com.cubeworld.rating;

import com.cubeworld.func.Observable;

/**
 * Created by fortknock on 24.06.16.
 */
public interface ObservableRatable extends Ratable, Observable<Double> {
}
