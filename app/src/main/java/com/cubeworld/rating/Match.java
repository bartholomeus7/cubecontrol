package com.cubeworld.rating;

/**
 * Created by fortknock on 17.01.16.
 */

public class Match {
    private final Ratable player1;
    private final Ratable player2;
    private final float timeSpend;
    private final MultipleChoiceResult result;


    public Match(Ratable player1, Ratable player2, MultipleChoiceResult result, float timeSpend) {
        this.player1 = player1;
        this.player2 = player2;
        this.result = result;
        this.timeSpend = timeSpend;
    }

    public Ratable getPlayer1() {
        return player1;
    }

    public Ratable getPlayer2() {
        return player2;
    }

    public MultipleChoiceResult getResult() {
        return result;
    }

    public float getTimeSpend() {
        return timeSpend;
    }
}
