package com.cubeworld.sound;

public enum CubeSoundEvent {
    CUBE_DROPPED(0), RIDDLE_SOLVED(1), RIDDLE_FAILED(1), TIME_OUT(1);

    final int priority;

    CubeSoundEvent(int priority) {
        this.priority = priority;
    }
}