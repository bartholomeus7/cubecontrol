package com.cubeworld.sound;

import android.media.AudioManager;
import android.media.SoundPool;

import com.cubeworld.func.Observable;
import com.cubeworld.func.Observer;
import com.cubeworld.cubes.PlayActivity;
import com.threed.jpct.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by fortknock on 24.06.16.
 */
public class CubeSoundPlayer implements Observer<CubeSoundEvent>, SoundPool.OnLoadCompleteListener {

    private static final int USELESS_FUTURE_COMPATIBLE_DEFAULT_SOUNDPOOL_PRIORITY = 1;
    private static final int LOAD_SUCCESS = 0;
    private static final int MAX_SOUND = 8;

    private SoundPool spool = initSoundPool();
    private Map<CubeSoundEvent, Integer> eventToSoundId = new HashMap<>();
    private Map<Integer, CubeSoundEvent> soundToEvent = new HashMap<>();

    private SoundPool initSoundPool() {
        final SoundPool pool;
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP) {
            pool = new SoundPool(MAX_SOUND, AudioManager.STREAM_MUSIC, 0);
        } else {
            pool = new SoundPool.Builder().setMaxStreams(MAX_SOUND).build();
        }
        pool.setOnLoadCompleteListener(this);
        return pool;
    }

    public void prepareSound(CubeSoundEvent event, int ressourceId) {
        int soundId = spool.load(PlayActivity.CONTEXT, ressourceId, USELESS_FUTURE_COMPATIBLE_DEFAULT_SOUNDPOOL_PRIORITY);
        soundToEvent.put(soundId, event);
    }

    @Override
    public void update(Observable<CubeSoundEvent> source, CubeSoundEvent soundEvent) {
        Integer soundId = eventToSoundId.get(soundEvent);
        if (soundId != null) {
            spool.play(soundId, 1f, 1f, soundEvent.priority, 0, 1f);
        }
    }


    public CubeSoundPlayer() {

    }


    @Override
    public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
        if (status == LOAD_SUCCESS) {
            eventToSoundId.put(soundToEvent.get(sampleId), sampleId);
        }
    }
}
