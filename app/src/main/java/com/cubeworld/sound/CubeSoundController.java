package com.cubeworld.sound;

import com.cubeworld.cubes.CubeWorldEvent;
import com.cubeworld.func.Observable;
import com.cubeworld.func.Observer;
import com.cubeworld.rating.Match;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fortknock on 25.06.16.
 */
public class CubeSoundController implements Observer<Object>, Observable<CubeSoundEvent> {
    private List<Observer<CubeSoundEvent>> observers = new ArrayList<>();


    @Override
    public void register(Observer<CubeSoundEvent> observer) {
        observers.add(observer);
    }

    @Override
    public boolean unregister(Observer<CubeSoundEvent> observer) {
        return observers.remove(observer);
    }


    private void informObservers(CubeSoundEvent event) {
        for (Observer<CubeSoundEvent> ob : observers) {
            ob.update(this, event);
        }
    }

    @Override
    public void update(Observable<Object> source, Object object) {
        if (object instanceof CubeWorldEvent) {
            switch ((CubeWorldEvent) object) {
                case CUBE_DROP:
                    informObservers(CubeSoundEvent.CUBE_DROPPED);
                    break;
            }
        }
        if (object instanceof Match) {
            Match m = (Match) object;
            switch (m.getResult()) {
                case RIGHT_ANSWER:
                    informObservers(CubeSoundEvent.RIDDLE_SOLVED);
                    break;
                case WRONG_ANSWER:
                    informObservers(CubeSoundEvent.RIDDLE_FAILED);
                    break;
                case PASS:
                    informObservers(CubeSoundEvent.TIME_OUT);
            }
        }
    }

    public <X> Observer<X> asObserver(Class<X> type) {
        return new Observer<X>() {
            @Override
            public void update(Observable<X> source, X object) {
                CubeSoundController.this.update((Observable<Object>) source, object);
            }
        };
    }
}
