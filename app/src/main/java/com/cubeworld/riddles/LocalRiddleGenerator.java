package com.cubeworld.riddles;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;

import com.cubeworld.misc.Preparable;
import com.cubeworld.func.Supplier;
import com.cubeworld.modes.TrainingMode;

/**
 * Created by fortknock on 08.01.16.
 */
public class LocalRiddleGenerator implements Supplier<CubeRiddle>, Preparable {
    private BlockingQueue<CubeRiddle> riddles = new LinkedBlockingDeque<>();
    private Executor tp = Executors.newSingleThreadExecutor();
    private Supplier<Integer> cubeCountSupplier = TrainingMode.DEFAULT_LOCAL_PLAY_MODE;
    private static int DEFAULT_CACHE = 5;

    private Runnable generateRiddle = new Runnable() {
        @Override
        public void run() {
            riddles.add(CubeRiddle.generateRiddle(cubeCountSupplier.get()));
        }
    };

    @Override
    public void prepare(Runnable finished) {
        for (int i = 0; i < DEFAULT_CACHE; i++) {
            riddles.add(CubeRiddle.generateRiddle(cubeCountSupplier.get()));
        }
        finished.run();
    }

    public LocalRiddleGenerator(Supplier<Integer> cubeCountSupplier) {
        this();
        if (cubeCountSupplier == null)
            throw new IllegalArgumentException("cubeCountSupplier can not be null");
        this.cubeCountSupplier = cubeCountSupplier;
    }

    public LocalRiddleGenerator() {

    }

    @Override
    public CubeRiddle get() {
        tp.execute(generateRiddle);
        try {
            return riddles.take();
        } catch (InterruptedException e) {
            throw new RuntimeException("Fuck this checked expception");
        }
    }
}
