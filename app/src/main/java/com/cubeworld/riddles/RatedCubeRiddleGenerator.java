package com.cubeworld.riddles;


import com.cubeworld.func.Observable;
import com.cubeworld.func.Observer;
import com.cubeworld.func.Supplier;
import com.cubeworld.json.JsonAsynchronPool;
import com.cubeworld.cubes.PlayActivity;
import com.cubeworld.main.R;
import com.cubeworld.misc.Dates;
import com.cubeworld.misc.GPersistable;
import com.cubeworld.misc.Persistable;
import com.cubeworld.misc.Preparable;
import com.cubeworld.misc.Settings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.threed.jpct.Logger;

import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by fortknock on 08.01.16.
 */

public class RatedCubeRiddleGenerator extends Persistable implements Supplier<CubeRiddle>, Observer<Double>, Preparable {

    private static final int RIDDLE_CACHE_SIZE = 10;
    private static final String RATING_SERVICE = "http://cube-1179.appspot.com/difficulties";

    private static final String CUBE_RIDDLE_COLLECTION_PROPERTY = "riddleCollection";

    private static final Type CUBE_RIDDLE_COLLECTION_TYPE = new TypeToken<Collection<CubeRiddle>>() {
    }.getType();
    private static final Type RATING_MAP_TYPE = new TypeToken<Map<Integer, Double>>() {
    }.getType();

    private final GPersistable<Date> lastUpdate = new GPersistable<>("lastupdate", new TypeToken<Date>() {
    }, new Date(0));
    private GPersistable<Map<Integer, Double>> rating_map = new GPersistable<>("rating_map", new TypeToken<Map<Integer, Double>>() {
    }, null);
    private GPersistable<List<CubeRiddle>> cachedRiddles = new GPersistable<List<CubeRiddle>>("10riddles", new TypeToken<List<CubeRiddle>>() {
    }, null);


    private static RatedCubeRiddleGenerator instance = null;
    private List<CubeRiddle> loadedRiddles = new ArrayList<>();
    private JsonAsynchronPool jpool = PlayActivity.getJSoonPool();


    private double currentRating = 1500;

    private RatedCubeRiddleGenerator() {
    }

    private void init() {
        if (cachedRiddles.get() != null) {
            loadedRiddles = cachedRiddles.get();
            //we have some cached riddles left, execute preparation in background while user is playing
            PlayActivity.getGlobalThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    prepareWellRatedRiddles();
                }
            });
        } else {
            // we have nothing cached, we have to wait for initliazation now
            prepareWellRatedRiddles();
        }
    }

    private void prepareWellRatedRiddles() {
        List<CubeRiddle> riddles = initRiddles();
        updateRatingsAndSetup(riddles);
    }


    private void updateRatingsAndSetup(List<CubeRiddle> riddles) {
        Collections.sort(riddles, CubeRiddle.COMPARE_BY_ID);
        if (shouldUpdateRatings()) {
            Logger.log("Tying to download net ratings!");
            try {
                Map<Integer, Double> ratingMap = (Map<Integer, Double>) jpool.getAsynchron(RATING_SERVICE, RATING_MAP_TYPE).get();
                rating_map.set(ratingMap);
                rating_map.persist();
            } catch (Exception e) {
            }
        }

        if (rating_map.get() != null) {
            for (Map.Entry<Integer, Double> entry : rating_map.get().entrySet()) {
                riddles.get(entry.getKey() - 1).setRating(entry.getValue());
            }
            Collections.sort(riddles, CubeRiddle.COMPARE_BY_ELO);
        }
        loadedRiddles = riddles;
        setUpRiddleCache();
    }

    private void setUpRiddleCache() {
        List<CubeRiddle> cache = new ArrayList<>();
        for (int i = 0; i < RIDDLE_CACHE_SIZE; i++) {
            cache.add(get());
        }
        cachedRiddles.set(cache);
        cachedRiddles.persist();
    }

    private boolean shouldUpdateRatings() {
        return Dates.isXDaysPast(lastUpdate.get(), Settings.get().getUpdate_frequency_days());
    }

    private List<CubeRiddle> initRiddles() {
        InputStreamReader reader = null;
        try {
            reader = new InputStreamReader(PlayActivity.CONTEXT.getResources().openRawResource(R.raw.ratedcubes));
            Gson gson = new Gson();
            return gson.fromJson(reader, CUBE_RIDDLE_COLLECTION_TYPE);
        } catch (Exception e) {
            throw new IllegalStateException("Fatal exception: Could not initlaize rated riddles from ressources.");
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e) {
                }
            }
        }
    }

    @Override
    public boolean persist() {
        return Persistable.persistToSharedPreferences(CUBE_RIDDLE_COLLECTION_PROPERTY, new ArrayList<>(loadedRiddles));
    }


    @Override
    public CubeRiddle get() {
        if (loadedRiddles.isEmpty()) {
            throw new IllegalStateException("Empty pool of rated riddles");
        }
        int nearest = Collections.binarySearch(loadedRiddles, CubeRiddle.dummyElo(currentRating), CubeRiddle.COMPARE_BY_ELO);
        //this is due to the strange return value of Collection.binarySearch
        if (nearest < 0) nearest = -1 - nearest;
        int range = loadedRiddles.size() / 8;
        Random r = new Random();
        int min = Math.min(Math.max(0, nearest - range), loadedRiddles.size() - 1 - 2 * range);
        int rand = min + r.nextInt(2 * range);
        return loadedRiddles.get(rand);
    }

    public static RatedCubeRiddleGenerator getInstance() {
        if (instance == null) {
            instance = new RatedCubeRiddleGenerator();
        }
        return instance;
    }


    @Override
    public void update(Observable<Double> source, Double object) {
        currentRating = object;
    }

    @Override
    public void prepare(Runnable finished) {

        if (loadedRiddles.isEmpty()) {
            init();
            finished.run();
        } else {
            finished.run();
        }
    }
}
