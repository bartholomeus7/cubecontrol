package com.cubeworld.riddles;

import android.os.SystemClock;

import com.cubeworld.rating.Ratable;
import com.threed.jpct.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import com.cubeworld.func.Functionals;
import com.cubeworld.func.GMap;
import com.cubeworld.func.Predicate;

/**
 * Created by fortknock on 26.12.15.
 */
public class CubeRiddle implements Ratable {


    public static final Comparator<CubeRiddle> COMPARE_BY_ELO = new Comparator<CubeRiddle>() {
        @Override
        public int compare(CubeRiddle lhs, CubeRiddle rhs) {
            return Double.compare(lhs.getRating(), rhs.getRating());
        }
    };

    public static final Comparator<CubeRiddle> COMPARE_BY_ID = new Comparator<CubeRiddle>() {
        @Override
        public int compare(CubeRiddle lhs, CubeRiddle rhs) {
            return (lhs.getId()<rhs.getId() ? -1 : (lhs.getId()==rhs.getId() ? 0 : 1));
        }
    };

    private static long GLOBAL_ID = 0;
    private static final long DEFAULT_SECONDS_TO_SOLVE = 10;
    private static final double DEFAULT_ELO = 1200;
    private static final double DEFAULT_ELO_INCREASE_PER_CUBE = 40;
    public static final int ANSWER_COUNT = 4;

    private long id = GLOBAL_ID++;

    private int solution;
    private List<Integer> options;

    private int width;
    private int depth;
    private int height;
    private List<Falling> intialPosition;

    private double elo;
    private long timeToSolve = TimeUnit.SECONDS.toMillis(DEFAULT_SECONDS_TO_SOLVE);

    private int totalPlays = 0;


    public CubeRiddle(int solution, List<Integer> options, int width, int depth, int height, List<Falling> intialPosition, double elo) {
        this.solution = solution;
        this.options = options;
        this.width = width;
        this.depth = depth;
        this.height = height;
        this.intialPosition = intialPosition;
        this.elo = elo;
    }

    public static Collection<Int3D> getRandomCubeDistribution(final int product) {
        final int maxCube = 1 + (int) Math.ceil(Math.pow(product, 1d / 3d));
        Logger.log("maxcube =  " + String.valueOf(maxCube));
        Int3D max = Int3D.getConstant(maxCube);
        Predicate<Int3D> fittingCube = Int3D.getIsBetween(Int3D.ZERO, max);
        Set<Int3D> result = new HashSet<Int3D>();
        result.add(new Int3D(0, 0, 1));
        for (int i = 0; i < product - 1; i++) {
            result.add(Int3D.getRandomNeighbor(result, Functionals.and(fittingCube, Int3D.hasButtom(result))));
        }

        if (!finalDistributionIsConsistent(result)) {
            throw new IllegalStateException("Inconsistent final distribution");
        }
        return normalize(result);
    }

    private static Collection<Int3D> normalize(Set<Int3D> int3Ds) {
        int minX = Collections.min(int3Ds, Int3D.COMPARE_X).x;
        int minY = Collections.min(int3Ds, Int3D.COMPARE_Y).y;
        int minZ = Collections.min(int3Ds, Int3D.COMPARE_Z).z;
        final Int3D min = new Int3D(minX, minY, minZ);
        return Functionals.mapEach(int3Ds, new GMap<Int3D>() {
            @Override
            public Int3D map(Int3D int3D) {
                return int3D.minus(min);
            }
        });
    }

    /**
     * @param finalD
     * @return
     */

    private static boolean finalDistributionIsConsistent(Collection<Int3D> finalD) {
        if (new HashSet<Int3D>(finalD).size() != finalD.size()) {
            throw new IllegalStateException("Double random cubes detected");
        }
        return true;
    }

    public static CubeRiddle generateRiddle(int num) {

        long x = SystemClock.elapsedRealtime();

        List<Int3D> finalDis = new ArrayList<>(getRandomCubeDistribution(num));
        int width = 1 + Collections.max(finalDis, Int3D.COMPARE_X).x;
        int depth = 1 + Collections.max(finalDis, Int3D.COMPARE_Z).z;
        int height = 1 + Collections.max(finalDis, Int3D.COMPARE_Y).y;

        Random r = new Random();

        Map<Int3D, Falling> intialPosition = new HashMap<>();
        double elo = DEFAULT_ELO + DEFAULT_ELO_INCREASE_PER_CUBE * num;

        Collections.sort(finalDis, Int3D.COMPARE_Y);
        for (Int3D i : finalDis) {
            Falling f = null;
            if (!intialPosition.containsKey(i.buttom())) {
                f = new Falling(i.x, -i.y - r.nextFloat() * 6, i.z, 2f + r.nextFloat() * 4f, -i.y);
            } else {
                Falling buttom = intialPosition.get(i.buttom());
                f = buttom.getSlowerFalling(-i.y, -i.y - r.nextFloat() * 6, 2f + r.nextFloat() * 4f);
            }

            intialPosition.put(i, f);
        }

        int pivotOptions = Math.max(1, num - r.nextInt(4));

        List<Integer> options = new ArrayList<>();
        for (int i = pivotOptions; i < pivotOptions + 4; i++) {
            options.add(i);
        }

        Logger.log("Milis to generate riddle : " + String.valueOf(SystemClock.elapsedRealtime() - x));

        return new CubeRiddle(num, options, width, depth, height, new ArrayList<Falling>(intialPosition.values()), elo);
    }

    public int getSolution() {
        return solution;
    }

    public List<Integer> getOptions() {
        return options;
    }

    public int getWidth() {
        return width;
    }

    public int getDepth() {
        return depth;
    }

    public int getHeight() {
        return height;
    }

    public List<Falling> getIntialPosition() {
        return intialPosition;
    }

    public long getId() {
        return id;
    }

    public double getElo() {
        return elo;
    }

    public long getTimeToSolve() {
        return timeToSolve;
    }


    public static CubeRiddle dummyElo(double elo) {
        return new CubeRiddle(0, null, 0, 0, 0, null, elo);
    }

    public void setRating(double rating) {
        this.elo = rating;
    }


    @Override
    public double getRating() {
        return elo;
    }

    @Override
    public double adaptRating(double plus) {
        return elo += plus;
    }

    @Override
    public double getWeight() {
        if (totalPlays < 50)
            return 40;
        else if (totalPlays < 200)
            return 30;
        else
            return 20;
    }

    @Override
    public String toString() {
        return "CubeRiddle{" +
                "id=" + id +
                ", solution=" + solution +
                ", options=" + options +
                ", width=" + width +
                ", depth=" + depth +
                ", height=" + height +
                ", intialPosition=" + intialPosition +
                ", elo=" + elo +
                ", timeToSolve=" + timeToSolve +
                ", totalPlays=" + totalPlays +
                '}';
    }
}
