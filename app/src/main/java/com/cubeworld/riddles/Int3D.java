package com.cubeworld.riddles;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import com.cubeworld.func.Functionals;
import com.cubeworld.func.GMap;
import com.cubeworld.func.Predicate;

class Int3D {
    public final int x;
    public final int y;
    public final int z;

    public final static Int3D ZERO = new Int3D(0, 0, 0);

    private static final int NEIGHBORS_COUNT = 6;

    public static final Comparator<Int3D> COMPARE_X = new Comparator<Int3D>() {
        @Override
        public int compare(Int3D lhs, Int3D rhs) {
            return lhs.x - rhs.x;
        }
    };

    public static final Comparator<Int3D> COMPARE_Y = new Comparator<Int3D>() {
        @Override
        public int compare(Int3D lhs, Int3D rhs) {
            return lhs.y - rhs.y;
        }
    };

    public static final Comparator<Int3D> COMPARE_Z = new Comparator<Int3D>() {
        @Override
        public int compare(Int3D lhs, Int3D rhs) {
            return lhs.z - rhs.z;
        }
    };


    public Int3D(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Int3D that = (Int3D) o;

        if (x != that.x) return false;
        if (y != that.y) return false;
        return z == that.z;

    }

    @Override
    public int hashCode() {
        int result = x ^ (x >>> 32);
        result = 31 * result + (y ^ (y >>> 32));
        result = 31 * result + (z ^ (z >>> 32));
        return result;
    }

    public Int3D generateRandomNeighbor() {
        Random r = new Random();
        List<Int3D> neighbors = generateNeighbors();
        return neighbors.get(r.nextInt(neighbors.size()));
    }

    public List<Int3D> generateNeighbors() {
        List<Int3D> result = new ArrayList<Int3D>(NEIGHBORS_COUNT);
        result.add(new Int3D(x + 1, y, z));
        result.add(new Int3D(x - 1, y, z));
        result.add(new Int3D(x, y + 1, z));
        result.add(new Int3D(x, y - 1, z));
        result.add(new Int3D(x, y, z + 1));
        result.add(new Int3D(x, y, z - 1));
        assert (result.size() == NEIGHBORS_COUNT) : "Not well defined neighborhood";
        return result;
    }

    public Int3D minus(Int3D other) {
        return new Int3D(x - other.x, y - other.y, z - other.z);
    }

    public Int3D buttom() {
        return new Int3D(x, y - 1, z);
    }

    public boolean isStrictlySmaller(Int3D other) {
        return x < other.x && y < other.y && z < other.z;
    }

    public boolean isBiggerEqualThan(Int3D other) {
        return x >= other.x && y >= other.y && z >= other.z;
    }

    public static Int3D getRandomNeighbor(Collection<Int3D> current, Predicate<Int3D> acceptor) {
        Set<Int3D> all = new HashSet<Int3D>();
        for (Int3D i : current) {
            all.addAll(i.generateNeighbors());
        }
        all.removeAll(current);

        List<Int3D> list = new ArrayList<Int3D>();
        for (Int3D i : all) {
            if (acceptor.accept(i)) {
                list.add(i);
            }
        }

        if (list.isEmpty()) {
            throw new IllegalStateException("Impossible to find further neighbors who are accepted by acceptor");
        }

        Random r = new Random();
        return list.get(r.nextInt(list.size()));
    }

    public static Predicate<Int3D> getIsBiggerEqualThan(final Int3D compare) {
        return new Predicate<Int3D>() {
            @Override
            public boolean accept(Int3D int3D) {
                return int3D.isBiggerEqualThan(compare);
            }
        };
    }

    public static Predicate<Int3D> getIsSmallerThan(final Int3D int3d) {
        return new Predicate<Int3D>() {
            @Override
            public boolean accept(Int3D int3D) {
                return int3D.isStrictlySmaller(int3d);
            }
        };
    }

    public static Predicate<Int3D> hasButtom(final Set<Int3D> vectors) {
        return new Predicate<Int3D>() {
            @Override
            public boolean accept(Int3D int3D) {
                return int3D.y == 0 || vectors.contains(int3D.buttom());
            }
        };
    }

    public static Int3D getConstant(final int x) {
        return new Int3D(x, x, x);
    }


    public static Predicate<Int3D> getIsBetween(Int3D min, Int3D max) {
        return Functionals.and(getIsBiggerEqualThan(min), getIsSmallerThan(max));
    }

    @Override
    public String toString() {
        return "Int3D{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
}