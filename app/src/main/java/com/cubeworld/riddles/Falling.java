package com.cubeworld.riddles;

import java.util.Comparator;

/**
 * Created by fortknock on 27.12.15.
 */
public class Falling {
    public final float startX;
    public final float startY;
    public final float startZ;
    public final float speedY;
    public final float stopY;

    public Falling(float startX, float startY, float startZ, float speedY, float stopY) {
        this.startX = startX;
        this.startY = startY;
        this.startZ = startZ;
        this.speedY = speedY;
        this.stopY = stopY;
    }

    public Falling scale(float factor) {
        return new Falling(startX * factor, startY * factor, startZ * factor, speedY * factor, stopY * factor);
    }

    @Override
    public String toString() {
        return "Falling{" +
                "startX=" + startX +
                ", startY=" + startY +
                ", startZ=" + startZ +
                ", speedY=" + speedY +
                ", stopY=" + stopY +
                '}';
    }

    public final static Comparator<Falling> COMPARE_Y = new Comparator<Falling>() {
        @Override
        public int compare(Falling lhs, Falling rhs) {
            return (int) Math.signum(lhs.stopY - rhs.stopY);
        }
    };

    /**
     * @param stopY
     * @return
     */

    public Falling getSlowerFalling(float stopY, float startYproposition, float speedProposition) {
        return new Falling(startX, Math.min(startY - 1, startYproposition), startZ, Math.min(speedY, speedProposition), stopY);
    }
}
