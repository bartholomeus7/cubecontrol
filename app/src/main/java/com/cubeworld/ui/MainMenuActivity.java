package com.cubeworld.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.PopupWindow;

import com.cubeworld.cubes.PlayActivity;
import com.cubeworld.main.R;
import com.cubeworld.modes.TrainingMode;

/**
 * Created by fortknock on 17.01.16.
 */
public class MainMenuActivity extends AppCompatActivity {


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TrainingMode.init(this.getResources());
        setContentView(R.layout.main_menu);
        initButtonListeners();
    }

    private class StartTrainingGame implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            final Intent intent = new Intent(MainMenuActivity.this, PlayActivity.class);
            PopupMenu popup = new PopupMenu(MainMenuActivity.this, v);
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    TrainingMode d = TrainingMode.parseFromLabel(item.getTitle().toString());
                    intent.putExtra("gamemode", d.toInt());
                    startActivity(intent);
                    return true;
                }
            });

            for (TrainingMode d : TrainingMode.values()) {
                popup.getMenu().add(d.getText());
            }

            popup.show();
            PopupWindow pw = new PopupWindow();
        }
    }

    private class StartSurvivalGame implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainMenuActivity.this, PlayActivity.class);
            intent.putExtra("gamemode", -2);
            startActivity(intent);
        }
    }

    private class StartRatedGame implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainMenuActivity.this, PlayActivity.class);
            intent.putExtra("gamemode", -1);
            startActivity(intent);
        }
    }

    private class ShowOptions implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainMenuActivity.this, SettingsActivity.class);
            startActivity(intent);
        }
    }

    private class ShowStatistics implements View.OnClickListener {

        @Override
        public void onClick(View v) {

        }
    }

    private class Quit implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            MainMenuActivity.this.finish();
        }
    }

    private void initButtonListeners() {
        findViewById(R.id.startRated).setOnClickListener(new StartRatedGame());
        findViewById(R.id.startSurvival).setOnClickListener(new StartSurvivalGame());
        findViewById(R.id.startTraining).setOnClickListener(new StartTrainingGame());
        findViewById(R.id.showOptions).setOnClickListener(new ShowOptions());
        findViewById(R.id.showStatistics).setOnClickListener(new ShowStatistics());
        findViewById(R.id.quit).setOnClickListener(new Quit());
    }

}
