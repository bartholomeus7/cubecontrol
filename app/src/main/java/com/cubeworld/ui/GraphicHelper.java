package com.cubeworld.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by fortknock on 03.01.16.
 */
public class GraphicHelper {

    private GraphicHelper() {
        throw new RuntimeException("Static utility class");
    }

    private static final int WHITE = Color.parseColor("#FFFFFFFF");
    //not so good practice but ok
    private static Map<View, ValueAnimator> ANIMATORS = new ConcurrentHashMap<>();

    public static void blink(final View view, final int duration, final ColorDrawable color) {
        if (ANIMATORS.containsKey(view)) {
            ANIMATORS.get(view).cancel();
        }

        shiftColor(view, duration / 2, WHITE, color.getColor(), new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                shiftColor(view, duration / 4, color.getColor(), WHITE, null);
            }
        });
    }

    private static void shiftColor(final View view, final int duration, int fromC, int toC, AnimatorListenerAdapter onFinish) {
        final float[] from = new float[3],
                to = new float[3];
        Color.colorToHSV(fromC, from);
        Color.colorToHSV(toC, to);
        final float[] hsv = new float[3];

        ValueAnimator anim = ValueAnimator.ofFloat(0, 1);
        anim.setDuration(duration);

        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                hsv[0] = from[0] + (to[0] - from[0]) * animation.getAnimatedFraction();
                hsv[1] = from[1] + (to[1] - from[1]) * animation.getAnimatedFraction();
                hsv[2] = from[2] + (to[2] - from[2]) * animation.getAnimatedFraction();
                ColorFilter filter = new PorterDuffColorFilter(Color.HSVToColor(hsv), PorterDuff.Mode.MULTIPLY);
                view.getBackground().setColorFilter(filter);
            }
        });
        if (onFinish != null) {
            anim.addListener(onFinish);
        }
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                ANIMATORS.remove(view);
            }
        });
        anim.start();
        ANIMATORS.put(view, anim);

    }


}
