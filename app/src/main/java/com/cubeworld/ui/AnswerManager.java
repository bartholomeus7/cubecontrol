package com.cubeworld.ui;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Button;

import com.cubeworld.func.Observable;
import com.cubeworld.func.Observer;
import com.cubeworld.main.R;
import com.cubeworld.riddles.CubeRiddle;
import com.threed.jpct.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by fortknock on 16.01.16.
 */
public class AnswerManager implements Observable<Integer>, Observer<CubeRiddle> {

    private CubeRiddle currentRiddle;
    private List<Observer<Integer>> observers = new ArrayList<>();
    private AtomicBoolean blockingClicks = new AtomicBoolean(true);

    private int[] answerIds = new int[]{R.id.answer1, R.id.answer2, R.id.answer3, R.id.answer4};
    private List<Button> answerButtons = new ArrayList<>();


    private final View.OnClickListener bListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            Button b = (Button) v;

            if (!blockingClicks.get()) {
                Integer answer = Integer.valueOf(b.getText().toString());
                if (answer.equals(currentRiddle.getSolution())) {
                    GraphicHelper.blink(b, 1000, new ColorDrawable(0xcc00cc00));
                } else {
                    GraphicHelper.blink(b, 1000, new ColorDrawable(0xcccc0000));
                }
                for (Observer<Integer> o : observers) {
                    o.update(AnswerManager.this, answer);
                }
            }
        }

    };

    public AnswerManager(Activity activity) {
        for (int id : answerIds) {
            answerButtons.add((Button) activity.findViewById(id));
        }
        for (Button b : answerButtons) {
            b.setOnClickListener(bListener);
        }
    }


    private void loadAnswers(final CubeRiddle r) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < CubeRiddle.ANSWER_COUNT; i++) {
                    answerButtons.get(i).setText(String.valueOf(r.getOptions().get(i)));
                }
                blockingClicks.set(false);
            }
        });
    }

    @Override
    public void register(Observer<Integer> observer) {
        observers.add(observer);
    }

    @Override
    public boolean unregister(Observer<Integer> observer) {
        return observers.remove(observer);
    }

    @Override
    public void update(Observable<CubeRiddle> source, CubeRiddle object) {
        blockingClicks.set(true);
        loadAnswers(object);
        currentRiddle = object;
    }

}
