package com.cubeworld.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.os.Handler;
import android.os.Looper;
import android.view.animation.LinearInterpolator;
import android.widget.ProgressBar;

import com.cubeworld.func.Observable;
import com.cubeworld.func.Observer;
import com.cubeworld.func.TimeScaling;
import com.cubeworld.cubes.CubeController;
import com.cubeworld.rating.MultipleChoiceResult;
import com.cubeworld.riddles.CubeRiddle;

import java.util.concurrent.atomic.AtomicBoolean;

public class ProgressBarController implements Observer<CubeRiddle> {
    private final ProgressBar bar;
    private final CubeController cc;
    private final TimeScaling scale;
    private ValueAnimator animator;

    public ProgressBarController(ProgressBar progress, CubeController cubecontrol, TimeScaling scale) {
        this.bar = progress;
        this.cc = cubecontrol;
        this.scale = scale;
    }

    @Override
    public void update(Observable<CubeRiddle> source, final CubeRiddle object) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                updateProgressbar(object);
            }
        });
    }

    private void updateProgressbar(final CubeRiddle riddle) {
        if (animator != null) {
            animator.cancel();
        }
        animator = animateProgress((int) (riddle.getTimeToSolve() / scale.getTimeScale()));

    }

    public ValueAnimator animateProgress(final int duration) {

        final ValueAnimator valueAnimator = ValueAnimator.ofInt(0, (duration));
        final AtomicBoolean hasBeenCancelled = new AtomicBoolean(false);
        valueAnimator.setDuration(duration);
        valueAnimator.setInterpolator(new LinearInterpolator());

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                bar.setProgress((Integer) valueAnimator.getAnimatedValue());
            }
        });
        valueAnimator.addListener(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationCancel(Animator animation) {
                hasBeenCancelled.set(true);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (!hasBeenCancelled.get()) cc.next(MultipleChoiceResult.PASS);
            }
        });


        bar.setMax(duration);
        valueAnimator.start();

        return valueAnimator;
    }
}