package com.cubeworld.ui;

import android.animation.ValueAnimator;
import android.os.Handler;
import android.os.Looper;
import android.widget.TextView;

import com.cubeworld.func.Observable;
import com.cubeworld.func.Observer;

import java.text.DecimalFormat;

/**
 * Created by fortknock on 20.01.16.
 */
public class RatingAdapter implements Observer<Double> {
    private final TextView tv;
    private static final DecimalFormat df = new DecimalFormat("#");
    private double currentShownvalue = -1;
    ValueAnimator currentAnimator = null;

    private static final int ANIMATION_DURATION_MILIS = 2000;

    public RatingAdapter(TextView tv, double initialValue) {
        this.tv = tv;
        tv.setText(df.format(initialValue));
        currentShownvalue = initialValue;
    }


    @Override
    public void update(Observable<Double> source, Double object) {
        if (currentAnimator != null) currentAnimator.cancel();
        currentAnimator = animateTextView(currentShownvalue, object);
    }


    public ValueAnimator animateTextView(double start, double finish) {

        final ValueAnimator valueAnimator = ValueAnimator.ofFloat((float) start, (float) finish);
        valueAnimator.setDuration(ANIMATION_DURATION_MILIS);

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        tv.setText(df.format(valueAnimator.getAnimatedValue()));
                        currentShownvalue = (Float) valueAnimator.getAnimatedValue();
                    }
                });
            }
        });

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                valueAnimator.start();
            }
        });
        return valueAnimator;
    }


}
