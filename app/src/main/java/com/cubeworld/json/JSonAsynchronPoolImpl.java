package com.cubeworld.json;

import com.cubeworld.func.Listener;
import com.google.gson.Gson;
import com.threed.jpct.Logger;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by fortknock on 12.01.16.
 */
public class JSonAsynchronPoolImpl implements JsonAsynchronPool {

    private static final int HTTP_OK_STATUSCODE = 200;
    private static final int DEFAULT_THREADS = 2;

    private final ExecutorService threadpool;

    private static JSonAsynchronPoolImpl sharedPool;

    private class HttpException extends Exception {
        public HttpException(String msg) {
            super(msg);
        }
    }

    public JSonAsynchronPoolImpl() {
        this(Executors.newFixedThreadPool(DEFAULT_THREADS));
    }

    public JSonAsynchronPoolImpl(ExecutorService executor) {
        this.threadpool = executor;
    }

    public JSonAsynchronPoolImpl(int threads) {
        this(Executors.newFixedThreadPool(threads));
    }

    private void checkRights() {

    }

    private <X> X parseJsonFromService(final String service, Type t) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) (new URL(service).openConnection());
        urlConnection.setRequestMethod("GET");
        int status = urlConnection.getResponseCode();
        Logger.log("STATUS = " + status);
        BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
        Gson gson = new Gson();
        X x = gson.fromJson(reader, t);
        Logger.log("Service : " + service + "ResponesCode = " + status + "Requestmethod:GET");
        urlConnection.disconnect();
        return x;
    }

    private void sendJson(final String service, Object o) throws Exception {
        Gson gson = new Gson();
        String json = gson.toJson(o);
        Logger.log(o.toString());
        Logger.log("JSON" + json);
        HttpURLConnection urlConnection = (HttpURLConnection) (new URL(service).openConnection());
        urlConnection.setRequestProperty("Content-Type", "application/json");
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Length", "" +
                Integer.toString(json.getBytes().length));
        urlConnection.setUseCaches(false);
        urlConnection.setDoInput(true);
        urlConnection.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(
                urlConnection.getOutputStream());
        wr.writeBytes(json);
        wr.flush();
        wr.close();
        int status = urlConnection.getResponseCode();
        Logger.log("Service : " + service + "ResponesCode = " + status);
        urlConnection.disconnect();
        if (status != HTTP_OK_STATUSCODE)
            throw new HttpException("Server did not respond with 200 OK.");
    }

    public <X> void getAsynchron(final String service, final Type type, final Listener<X, Throwable> callback) {
        threadpool.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    X x = parseJsonFromService(service, type);
                    callback.action(x, null);
                } catch (Exception e) {
                    callback.action(null, e);
                }
            }
        });
    }

    public void sendAsynchron(final String service, final Object o, final Listener<Object, Throwable> callback) {
        threadpool.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    sendJson(service, o);
                    if (callback != null) {
                        callback.action(o, null);
                    }
                } catch (Throwable t) {
                    if (callback != null)
                        callback.action(o, t);
                }
            }
        });
    }

    public void sendAsynchron(final String service, final Object o) {
        sendAsynchron(service, o, null);
    }

    @Override
    public <X> Future<X> getAsynchron(final String service, final Type type) {
        return threadpool.submit(new Callable<X>() {
            @Override
            public X call() throws Exception {
                return parseJsonFromService(service, type);
            }
        });
    }


}
