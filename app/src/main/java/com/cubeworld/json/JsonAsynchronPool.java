package com.cubeworld.json;

import com.cubeworld.func.Listener;

import java.lang.reflect.Type;
import java.util.concurrent.Future;

/**
 * Created by fortknock on 06.02.16.
 */
public interface JsonAsynchronPool {
    <X> void getAsynchron(final String service, final Type type, final Listener<X, Throwable> callback);

    void sendAsynchron(final String service, final Object o, final Listener<Object, Throwable> callback);

    void sendAsynchron(final String service, final Object o);

    <X> Future<X> getAsynchron(final String service, final Type type);
}
