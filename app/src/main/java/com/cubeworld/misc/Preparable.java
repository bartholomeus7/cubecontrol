package com.cubeworld.misc;

/**
 * Created by fortknock on 25.01.16.
 */
public interface Preparable {
    void prepare(Runnable finished);
}
