package com.cubeworld.misc;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.cubeworld.cubes.PlayActivity;
import com.cubeworld.main.R;
import com.threed.jpct.Logger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by fortknock on 30.12.15.
 */
public class Settings implements SharedPreferences.OnSharedPreferenceChangeListener {


    /**
     * We use reflection to init and manage our settings, as this is more stable and less repeating.
     */

    @Retention(RetentionPolicy.RUNTIME)
    @interface Setting {
        int stringid();
    }

    private static Settings instance = null;

    @Setting(stringid = R.string.setting_wifi)
    private boolean only_use_wifi = false;

    @Setting(stringid = R.string.setting_riddle_frequency)
    private int update_frequency_days;

    private int rated_riddle_max_cache;

    @Setting(stringid = R.string.setting_statistic_cache)
    private int statistic_max_cache;

    @Setting(stringid = R.string.setting_statistic_frequency)
    private int statistics_send_interval;

    private boolean sound_effets_on = true;


    private static Map<String, Field> SETTING_MAP = getSettingMap();

    private static Map<String, Field> getSettingMap() {
        Map<String, Field> result = new HashMap<>();
        Field[] fields = Settings.class.getDeclaredFields();
        for (Field f : fields) {
            if (f.getAnnotation(Setting.class) != null) {
                Setting s = f.getAnnotation(Setting.class);
                f.setAccessible(true);
                result.put(PlayActivity.getInstance().getResources().getString(s.stringid()), f);
            }
        }
        return result;
    }


    public int getStatistics_send_interval() {
        return statistics_send_interval;
    }

    public int getStatistic_max_cache() {
        return statistic_max_cache;
    }

    public int getUpdate_frequency_days() {
        return update_frequency_days;
    }

    public int getRated_riddle_max_cache() {
        return rated_riddle_max_cache;
    }


    public boolean isSound_effets_on() {
        return sound_effets_on;
    }

    public boolean isOnly_use_wifi() {
        return only_use_wifi;
    }

    private Settings() {
        //singleton design pattern
    }

    public static Settings get() {
        if (instance == null) instance = load();
        return instance;
    }

    private static Settings load() {
        instance = new Settings();
        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(PlayActivity.CONTEXT);
        mPrefs.registerOnSharedPreferenceChangeListener(instance);
        for (Map.Entry<String, Field> entry : SETTING_MAP.entrySet()) {
            Field f = entry.getValue();
            Object o = getObject(mPrefs, entry.getKey(), f.getType());
            instance.set(f, o);
        }
        return instance;
    }

    private void set(Field f, Object o) {
        if (o == null) {
            return;
        }
        f.setAccessible(true);
        try {
            f.set(this, o);
        } catch (IllegalAccessException ae) {
            Logger.log(ae);
        }
    }


    /**
     * Ultra ugly method but neccessary due to the bad design of the shared preference class.
     * (?Why the fuck dont they have an get Object method, or at least convert to String if i ask them for a string??)
     * This method is pure frustration and hopefully will never be looked upon again in the lifetime of the programmer who wrote it.
     *
     * @param prefs
     * @param key
     * @return
     */
    private static Object getObject(SharedPreferences prefs, String key, Class target) {
        if (target == String.class) {
            return prefs.getString(key, null);
        }
        try {
            if (target == Boolean.class || target == boolean.class) {
                return prefs.getBoolean(key, false);
            }
            if (target == Integer.class || target == int.class) {
                return prefs.getInt(key, 0);
            }
            if (target == Long.class || target == long.class) {
                return prefs.getLong(key, 0);
            }
            if (target == Float.class || target == float.class) {
                return prefs.getFloat(key, 0);
            }
            if (target.isAssignableFrom(Set.class)) {
                return prefs.getStringSet(key, null);
            }
        } catch (Exception e) {
            //try to convert the string so a fitting primitive
            return toObject(target, prefs.getString(key, null));
        }
        throw new IllegalStateException("Unreachable");
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (SETTING_MAP.containsKey(key)) {
            set(SETTING_MAP.get(key), getObject(sharedPreferences, key, SETTING_MAP.get(key).getType()));
        }
    }

    private static Object toObject(Class clazz, String value) {
        if (Boolean.class == clazz || boolean.class == clazz) return Boolean.parseBoolean(value);
        if (Byte.class == clazz || byte.class == clazz) return Byte.parseByte(value);
        if (Short.class == clazz || short.class == clazz) return Short.parseShort(value);
        if (Integer.class == clazz || int.class == clazz) return Integer.parseInt(value);
        if (Long.class == clazz || long.class == clazz) return Long.parseLong(value);
        if (Float.class == clazz || float.class == clazz) return Float.parseFloat(value);
        if (Double.class == clazz || double.class == clazz) return Double.parseDouble(value);
        return value;
    }
}
