package com.cubeworld.misc;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.concurrent.ExecutorService;

/**
 * Wrapper arround some to-persist instance of type T, which persists to shared preference.
 *
 *
 * @param <T>
 */


public class GPersistable<T> extends Persistable {

    private T item;
    private final Type type;
    private final String id;
    private volatile boolean isInitliazed = false;
    private T defaultItem;


    public GPersistable(String id, TypeToken<T> typeToken) {
        this(id, typeToken, null);
    }


    public GPersistable(String id, TypeToken<T> typeToken, T defaultItem) {
        this.type = typeToken.getType();
        this.id = id;
        this.defaultItem = defaultItem;
    }

    private void init() {
        try {
            this.item = Persistable.getFromSharedPreferences(id, type);
        } catch (Exception e) {
            this.item = defaultItem;
        }
        isInitliazed = true;
    }

    public boolean isInitliazed() {
        return isInitliazed;
    }

    public T get() {
        if (!isInitliazed) init();
        return item;
    }

    public void set(T item) {
        this.item = item;
        isInitliazed |= true;
    }

    public boolean persist() {
        if (!isInitliazed) return false;
        return Persistable.persistToSharedPreferences(id, item);
    }


}

