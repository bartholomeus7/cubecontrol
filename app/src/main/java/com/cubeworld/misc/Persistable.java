package com.cubeworld.misc;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.cubeworld.cubes.PlayActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;

/**
 * Created by fortknock on 15.01.16.
 */
public abstract class Persistable {

    private static final String NO_OBJECT = "NO_OBJECT";

    public abstract boolean persist();

    public static class NoPersistedInstanceException extends Exception {
        public NoPersistedInstanceException(String msg) {
            super(msg);
        }
    }

    public static boolean persistToSharedPreferences(String property, Object toSave) {
        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(PlayActivity.CONTEXT);
        SharedPreferences.Editor ed = mPrefs.edit();
        Gson gson = new Gson();
        ed.putString(property, gson.toJson(toSave));
        ed.commit();
        return true;
    }

    public static <X> X getFromSharedPreferences(String property, Type type) throws NoPersistedInstanceException {
        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(PlayActivity.CONTEXT);
        String serialized = mPrefs.getString(property, NO_OBJECT);
        if (serialized.equals(NO_OBJECT)) {
            throw new NoPersistedInstanceException("No such object serialized.");
        }
        Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.TRANSIENT).create();
        return gson.fromJson(serialized, type);
    }


}
