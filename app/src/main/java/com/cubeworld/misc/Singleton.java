package com.cubeworld.misc;

/**
 * Created by fortknock on 05.02.16.
 */
public class Singleton<X> {
    private X item;
    private boolean finalize = false;

    public void set(X x) {
        if (finalize) throw new RuntimeException("Item already assigned and finalized.");
        this.item = x;
    }

    public X get() {
        return item;
    }

    public void finalize() {
        finalize = true;
    }

    public boolean isEmpty() {
        return item == null;
    }

}
