package com.cubeworld.misc;

import com.cubeworld.func.TimeScaling;

/**
 * Created by fortknock on 24.06.16.
 */
public class ScalableTime implements TimeScaling {
    private static float DEFAULT_SCALE = 1;

    private float scale;

    public ScalableTime() {
        this(DEFAULT_SCALE);
    }

    public ScalableTime(float scale) {
        this.scale = scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    @Override
    public float getTimeScale() {
        return scale;
    }
}
