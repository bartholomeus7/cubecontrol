package com.cubeworld.misc;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;

/**
 * Created by fortknock on 25.06.16.
 */
public class Uris {
    private Uris() {
        throw new RuntimeException("Static utility class");
    }

    public static Uri ResourceToUri(Context context, int resID) {
        return Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" +
                context.getResources().getResourcePackageName(resID) + '/' +
                context.getResources().getResourceTypeName(resID) + '/' +
                context.getResources().getResourceEntryName(resID));
    }
}
