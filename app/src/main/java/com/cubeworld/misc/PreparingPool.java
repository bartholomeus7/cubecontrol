package com.cubeworld.misc;

import android.app.ProgressDialog;

import com.cubeworld.misc.Preparable;
import com.threed.jpct.Logger;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by fortknock on 25.01.16.
 * <p>
 * Wrapping preparing class for preparing multiple preparables.
 */
public class PreparingPool implements Preparable {

    private final List<Preparable> preparables;
    private final ProgressDialog dialog;
    private final ExecutorService executor;
    private final boolean externExecutor;

    public PreparingPool(List<Preparable> preparables, ProgressDialog dialog) {
        this(preparables, dialog, Executors.newCachedThreadPool(), false);
    }

    public PreparingPool(List<Preparable> preparables, ProgressDialog dialog, ExecutorService executor) {
        this(preparables, dialog, executor, true);
    }

    private PreparingPool(List<Preparable> preparables, ProgressDialog dialog, ExecutorService executor, boolean extern) {
        this.preparables = preparables;
        this.dialog = dialog;
        this.executor = executor;
        this.externExecutor = extern;
    }


    @Override
    public void prepare(final Runnable finished) {
        prepareRessources(preparables, finished);
    }

    private void prepareRessources(final List<Preparable> preparables, Runnable finished) {
        final CountDownLatch waiter = new CountDownLatch(preparables.size());
        for (final Preparable p : preparables) {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        p.prepare(new Runnable() {
                            @Override
                            public void run() {
                                waiter.countDown();
                                int progress = preparables.size() - (int) waiter.getCount();
                                dialog.setProgress(progress);
                            }
                        });
                    } catch (Exception e) {
                        Logger.log(e);
                    }
                }
            });
        }

        try {
            waiter.await();
            dialog.dismiss();
            finished.run();
        } catch (InterruptedException e) {
            throw new RuntimeException("Could not prepare ressources");
        } finally {
            if (!externExecutor) {
                executor.shutdown();
            }
        }
    }
}
