package com.cubeworld.misc;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by fortknock on 23.06.16.
 */
public class Dates {


    private Dates() {
        throw new RuntimeException("Static utility class");
    }

    public static boolean isXDaysPast(Date date, int x) {
        Date lastupdate = date;
        Calendar cal = Calendar.getInstance();
        cal.setTime(lastupdate);
        cal.add(Calendar.DAY_OF_YEAR, Settings.get().getUpdate_frequency_days());
        return new Date().compareTo(cal.getTime()) > 0;

    }
}
