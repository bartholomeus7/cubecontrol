package com.cubeworld.modes;

import android.widget.TextView;

import com.cubeworld.func.Interrupting;
import com.cubeworld.func.Observer;
import com.cubeworld.func.Supplier;
import com.cubeworld.func.TimeScaling;
import com.cubeworld.cubes.PlayActivity;
import com.cubeworld.main.R;
import com.cubeworld.misc.GPersistable;
import com.cubeworld.misc.Persistable;
import com.cubeworld.rating.MultipleChoiceElo;
import com.cubeworld.rating.Match;
import com.cubeworld.rating.EloPlayer;
import com.cubeworld.rating.ObservableRatable;
import com.cubeworld.rating.StatisticManager;
import com.cubeworld.riddles.CubeRiddle;
import com.cubeworld.riddles.RatedCubeRiddleGenerator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by fortknock on 05.02.16.
 */
public class RatedModus implements GameMode {
    @Override
    public List<Interrupting> getInterruptings() {
        return Collections.emptyList();
    }

    public static final int PLAYMODE_ID = -1;

    private final static String label = PlayActivity.CONTEXT.getResources().getString(R.string.gamemode_rated);
    private final GPersistable<EloPlayer> player = EloPlayer.PlayerType.RATED.get();

    public RatedModus() {
        player.get().register(RatedCubeRiddleGenerator.getInstance());
    }

    @Override
    public void styleLabel(TextView view) {
        view.setText(label);
    }

    @Override
    public Supplier<CubeRiddle> getRiddleSupplier() {
        return RatedCubeRiddleGenerator.getInstance();
    }

    @Override
    public ObservableRatable getPlayer() {
        return player.get();
    }


    @Override
    public List<Persistable> getPersistables() {
        return Arrays.asList(RatedCubeRiddleGenerator.getInstance(), player);
    }

    @Override
    public TimeScaling getTimeScaling() {
        return TimeScaling.STANDARD_TIMESCALE;
    }

    @Override
    public List<Observer<Match>> getMatchObservers() {
        return Arrays.asList(new MultipleChoiceElo(), StatisticManager.getInstance());
    }

    @Override
    public void prepare(Runnable finished) {
        RatedCubeRiddleGenerator.getInstance().prepare(finished);
    }

}
