package com.cubeworld.modes;

import android.widget.TextView;

import com.cubeworld.func.Interrupting;
import com.cubeworld.func.Observer;
import com.cubeworld.func.Supplier;
import com.cubeworld.func.TimeScaling;
import com.cubeworld.misc.Persistable;
import com.cubeworld.misc.ScalableTime;
import com.cubeworld.rating.Match;
import com.cubeworld.rating.ObservableRatable;
import com.cubeworld.riddles.CubeRiddle;

import java.util.Collections;
import java.util.List;

/**
 * Currently under development
 */
public class LiveGame implements GameMode {
    @Override
    public void styleLabel(TextView view) {
        view.setText("Livegame");
    }

    @Override
    public Supplier<CubeRiddle> getRiddleSupplier() {
        return null;
    }

    @Override
    public ObservableRatable getPlayer() {
        return null;
    }

    @Override
    public List<Persistable> getPersistables() {
        return null;
    }

    @Override
    public TimeScaling getTimeScaling() {
        return new ScalableTime(1);
    }

    @Override
    public List<Observer<Match>> getMatchObservers() {
        return Collections.emptyList();
    }

    @Override
    public List<Interrupting> getInterruptings() {
        return Collections.emptyList();
    }

    @Override
    public void prepare(Runnable finished) {

    }

}
