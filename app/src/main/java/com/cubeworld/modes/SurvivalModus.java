package com.cubeworld.modes;

import android.widget.TextView;

import com.cubeworld.func.Interrupting;
import com.cubeworld.func.Observable;
import com.cubeworld.func.Observer;
import com.cubeworld.func.Supplier;
import com.cubeworld.func.TimeScaling;
import com.cubeworld.cubes.PlayActivity;
import com.cubeworld.main.R;
import com.cubeworld.misc.Persistable;
import com.cubeworld.rating.Match;
import com.cubeworld.rating.MultipleChoiceResult;
import com.cubeworld.rating.ObservableRatable;
import com.cubeworld.riddles.CubeRiddle;
import com.cubeworld.riddles.LocalRiddleGenerator;
import com.threed.jpct.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by fortknock on 24.06.16.
 */
public class SurvivalModus implements GameMode, Observer<Match>, Interrupting, Supplier<Integer>, TimeScaling, Observable<Double> {


    private List<Observer<Double>> observers = new ArrayList<>();

    public static final int PLAYMODE_ID = -2;

    private final static String label = PlayActivity.CONTEXT.getResources().getString(R.string.gamemode_survival);

    private int totalRiddles = 0;
    private float difficulty = 0f;

    private static final float MAX_DIFFICULTY = 600;

    /**
     * similar to mp elo, the idea is that the bias(deltadifficulty(guessing)) = bias(deltadifficulty(pass)),
     * so -3/4* WRONG_ANSWER_FACOTR + 1/4 *1f = -1 = factor_delta_pass
     */
    private static final float WRONG_ANSWER_FACOTR = 1.6666f;
    private static final float MIN_TIME_SCALE = 1f;
    private static final float MAX_TIME_SCALE = 3f;

    private float totalTime = 0;

    private static final int MIN_BASE = 5;
    private static final int MAX_BASE = 13;
    private static final int MIN_RANGE = 10;
    private static final int MAX_RANGE = 18;

    private static final float MAX_TIME_WISE_DIFFICULTY = 400;
    //after 6 minutes we will always reach max diffulty
    private static final float TIME_TO_MAX_TIME_WISE_DIFFICULTY = 50 * 6;

    private ObservableRatable player = new ObservableRatable() {

        private int cubes = 0;
        private List<Observer<Double>> observers = new ArrayList<>();

        @Override
        public double getRating() {
            return cubes;
        }

        @Override
        public double adaptRating(double plus) {
            cubes += (int) plus;
            for (Observer<Double> ob : observers) {
                ob.update(this, (double) cubes);
            }
            return cubes;
        }

        @Override
        public double getWeight() {
            throw new IllegalStateException("Should not be called");
        }

        @Override
        public void register(Observer<Double> observer) {
            observers.add(observer);
        }

        @Override
        public boolean unregister(Observer<Double> observer) {
            return observers.remove(observer);
        }
    };

    @Override
    public void styleLabel(TextView view) {
        view.setText(label);
    }

    @Override
    public Supplier<CubeRiddle> getRiddleSupplier() {
        return new LocalRiddleGenerator(this);
    }

    @Override
    public ObservableRatable getPlayer() {
        return player;
    }

    @Override
    public List<Persistable> getPersistables() {
        return Collections.emptyList();
    }

    @Override
    public TimeScaling getTimeScaling() {
        return this;
    }

    @Override
    public List<Observer<Match>> getMatchObservers() {
        return Collections.singletonList((Observer<Match>) this);
    }

    @Override
    public void prepare(Runnable finished) {
        finished.run();
    }

    @Override
    public void update(Observable<Match> source, Match match) {
        if (match.getPlayer2().getClass() != CubeRiddle.class) {
            throw new IllegalArgumentException("Excepting a cube riddle as second argument");
        }

        totalRiddles++;
        CubeRiddle riddle = (CubeRiddle) match.getPlayer2();
        if (match.getResult()== MultipleChoiceResult.RIGHT_ANSWER){
            match.getPlayer1().adaptRating(riddle.getSolution());
        }

        totalTime += match.getTimeSpend();

        switch (match.getResult()) {
            case PASS:
                difficulty += Math.sqrt(totalRiddles);
                break;
            case WRONG_ANSWER:
                difficulty += WRONG_ANSWER_FACOTR * Math.sqrt(totalRiddles);
                break;
            case RIGHT_ANSWER:
                difficulty -= 0.25f * Math.sqrt(totalRiddles);
                break;
        }
        Logger.log("Difficulty = " + difficulty + "time = " + totalTime + "");
        difficulty = fit(difficulty, getTimeWiseDifficulty(), MAX_DIFFICULTY);
    }

    private static float fit(float x, float min, float max) {
        return x > max ? x : x < min ? min : x;
    }

    @Override
    public boolean possibleInterrupt(Runnable onInterruptFinish) {
        return false;
    }

    @Override
    public Integer get() {
        Random r = new Random();
        int result = (int) (difNormed(MIN_BASE, MAX_BASE));
        result += r.nextInt((int) difNormed(MIN_RANGE, MAX_RANGE));
        return result;
    }

    @Override
    public float getTimeScale() {
        return difNormed(MIN_TIME_SCALE, MAX_TIME_SCALE);
    }

    private float difNormed(float min, float max) {
        return min + (max - min) * (difficulty / MAX_DIFFICULTY);
    }

    private float getTimeWiseDifficulty() {
        return Math.min(MAX_TIME_WISE_DIFFICULTY, MAX_TIME_WISE_DIFFICULTY * totalTime / TIME_TO_MAX_TIME_WISE_DIFFICULTY);
    }

    @Override
    public List<Interrupting> getInterruptings() {
        return Collections.singletonList((Interrupting) this);
    }

    @Override
    public void register(Observer<Double> observer) {
        observers.add(observer);
    }

    @Override
    public boolean unregister(Observer<Double> observer) {
        return observers.remove(observer);
    }
}
