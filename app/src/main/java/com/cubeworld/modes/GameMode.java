package com.cubeworld.modes;

import android.widget.TextView;

import com.cubeworld.func.Interrupting;
import com.cubeworld.func.Observer;
import com.cubeworld.func.Supplier;
import com.cubeworld.func.TimeScaling;
import com.cubeworld.misc.Persistable;
import com.cubeworld.misc.Preparable;
import com.cubeworld.rating.Match;
import com.cubeworld.rating.ObservableRatable;
import com.cubeworld.riddles.CubeRiddle;

import java.util.List;

/**
 * Created by fortknock on 05.02.16.
 */
public interface GameMode extends Preparable {
    void styleLabel(TextView view);

    Supplier<CubeRiddle> getRiddleSupplier();

    ObservableRatable getPlayer();

    List<Persistable> getPersistables();

    TimeScaling getTimeScaling();

    List<Observer<Match>> getMatchObservers();

    List<Interrupting> getInterruptings();


}
