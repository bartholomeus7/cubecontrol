package com.cubeworld.modes;

import android.content.res.Resources;
import android.widget.TextView;

import com.cubeworld.func.Interrupting;
import com.cubeworld.func.Observer;
import com.cubeworld.func.TimeScaling;
import com.cubeworld.misc.GPersistable;
import com.cubeworld.misc.Persistable;
import com.cubeworld.misc.ScalableTime;
import com.cubeworld.misc.Singleton;
import com.cubeworld.func.Supplier;
import com.cubeworld.main.R;
import com.cubeworld.rating.MultipleChoiceElo;
import com.cubeworld.rating.Match;
import com.cubeworld.rating.EloPlayer;
import com.cubeworld.rating.ObservableRatable;
import com.cubeworld.riddles.CubeRiddle;
import com.cubeworld.riddles.LocalRiddleGenerator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by fortknock on 30.01.16.
 */
public enum TrainingMode implements GameMode, Supplier<Integer> {


    EASY(2, 7, R.string.dif1), MEDIUM(6, 11, R.string.dif2), HARD(9, 14, R.string.dif3), IMPOSSIBLE(13, 18, R.string.dif4), HELLISH(15, 25, R.string.dif5);

    private static final GPersistable<EloPlayer> PLAYER = EloPlayer.PlayerType.TRAIN.get();
    public static final TrainingMode DEFAULT_LOCAL_PLAY_MODE = MEDIUM;


    private int min;
    private int range;
    private int resId;
    private Singleton<String> label = new Singleton<>();


    public static void init(Resources r) {
        for (TrainingMode l : values()) {
            l.label.set(r.getString(l.resId) + " " + r.getString(R.string.gamemode_training));
            l.finalize();
        }
    }

    TrainingMode(int min, int range, int stringRessource) {
        Random r = new Random();
        this.min = Math.max(1, min - 2 + r.nextInt(4));
        this.range = range - 2 + r.nextInt(4);
        this.resId = stringRessource;
    }

    @Override
    public void prepare(Runnable finished) {
        finished.run();
    }

    public int getCount() {
        Random r = new Random();
        return min + r.nextInt(range);
    }

    public static TrainingMode parseFromInt(int d) {
        if (d < 0 || d >= TrainingMode.values().length)
            throw new IllegalArgumentException("No Local play mode for this id exists");
        return TrainingMode.values()[d];
    }

    @Override
    public void styleLabel(TextView view) {
        view.setText(getText());
    }

    public String getText() {
        if (label.isEmpty())
            throw new RuntimeException("Game modes not initialized via static init call.");
        return label.get();
    }

    @Override
    public Supplier<CubeRiddle> getRiddleSupplier() {
        return new LocalRiddleGenerator(this);
    }

    @Override
    public ObservableRatable getPlayer() {
        return PLAYER.get();
    }

    @Override
    public List<Persistable> getPersistables() {
        return Arrays.asList((Persistable) PLAYER);
    }

    @Override
    public TimeScaling getTimeScaling() {
        return new ScalableTime(2);
    }

    @Override
    public List<Observer<Match>> getMatchObservers() {
        return Arrays.asList((Observer<Match>) new MultipleChoiceElo());
    }

    public int toInt() {
        return Arrays.asList(TrainingMode.values()).indexOf(this);
    }

    public static TrainingMode parseFromLabel(String label) {
        for (TrainingMode pm : values()) {
            if (pm.label.get().equals(label)) return pm;
        }
        throw new IllegalArgumentException("No local play mode with this name :" + label);
    }

    @Override
    public Integer get() {
        return getCount();
    }

    @Override
    public List<Interrupting> getInterruptings() {
        return Collections.emptyList();
    }

}
