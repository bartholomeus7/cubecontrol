package com.cubeworld.ad;

import android.os.SystemClock;

import com.cubeworld.func.Interrupting;
import com.cubeworld.func.StopResumable;
import com.cubeworld.cubes.PlayActivity;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.threed.jpct.Logger;

/**
 * Created by fortknock on 25.01.16.
 */
public class AdManager implements Interrupting, StopResumable {
    private final static long AD_INTERVAL_MILIS = 1000 * 60 * 2;

    private long lastShow = SystemClock.elapsedRealtime();
    private long lastStopped = SystemClock.elapsedRealtime();

    private final InterstitialAd mInterstitialAd;
    private static AdManager instance;

    private AdManager() {
        mInterstitialAd = new InterstitialAd(PlayActivity.CONTEXT);
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        requestNewInterstitial();
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();

        mInterstitialAd.loadAd(adRequest);
    }

    public static AdManager get() {
        if (instance == null) {
            instance = new AdManager();
        }
        return instance;
    }

    @Override
    public boolean possibleInterrupt(final Runnable onInterruptFinish) {
        boolean enoughTimePassed = SystemClock.elapsedRealtime() - lastShow > AD_INTERVAL_MILIS;
        Logger.log(String.valueOf(mInterstitialAd.isLoaded()));
        boolean willShow = mInterstitialAd.isLoaded() & enoughTimePassed;
        if (willShow) {
            Logger.log("Showing ad");
            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    requestNewInterstitial();
                    onInterruptFinish.run();
                }
            });
            mInterstitialAd.show();
            lastShow = SystemClock.elapsedRealtime();

        } else {
            onInterruptFinish.run();
        }
        return willShow;
    }

    @Override
    public void onResumeDo() {
        lastShow += (SystemClock.elapsedRealtime() - lastStopped);
    }

    @Override
    public void onStopDo() {
        lastStopped = SystemClock.elapsedRealtime();
    }
}
