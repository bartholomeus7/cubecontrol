package com.cubeworld.cubes;

import android.opengl.GLSurfaceView;
import android.os.SystemClock;

import com.cubeworld.func.Observable;
import com.cubeworld.func.Observer;
import com.cubeworld.func.Pool;
import com.cubeworld.func.TimeScaling;
import com.cubeworld.main.R;
import com.cubeworld.misc.Preparable;
import com.cubeworld.riddles.CubeRiddle;
import com.threed.jpct.FrameBuffer;
import com.threed.jpct.Logger;
import com.threed.jpct.Object3D;
import com.threed.jpct.Texture;
import com.threed.jpct.TextureManager;
import com.threed.jpct.util.MemoryHelper;

import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

class CubeWorldRenderer implements GLSurfaceView.Renderer, Observable<CubeWorld>, Observer<CubeRiddle>, Preparable {

    private final PlayActivity master;

    private final TimeScaling timescale;
    private long time = SystemClock.elapsedRealtime();
    private long lastTime = SystemClock.elapsedRealtime();
    private int fps = 0;
    private float currentScale = 1f;

    private CubeWorld world = null;
    private FrameBuffer fb = null;

    private static final int[] textureIds = concatArrays(new int[]{R.raw.background}, CubePool.getTextureIdsUsed());

    private final List<Observer<CubeWorld>> observerList = new ArrayList<>();

    private static final int CUBE_SIZE = 40;
    private static final float SCALE = 5f;
    private Pool.PreparablePool<Object3D> generator = new CubePool(SCALE, CUBE_SIZE);

    private static int[] concatArrays(int[] array1, int[] array2) {
        int[] array1and2 = new int[array1.length + array2.length];
        System.arraycopy(array1, 0, array1and2, 0, array1.length);
        System.arraycopy(array2, 0, array1and2, array1.length, array2.length);
        return array1and2;
    }

    private void loadTextures() {
        for (int tid : textureIds) {
            String name = master.getResources().getResourceName(tid);
            if (!TextureManager.getInstance().containsTexture(name)) {
                try {
                    TextureManager.getInstance().addTexture(name, new Texture(PlayActivity.CONTEXT.getResources().openRawResource(tid)
                    ));
                } catch (Throwable t) {
                    throw new RuntimeException("Unable to load texture " + name, t);
                }
            }
        }
    }

    public CubeWorldRenderer(PlayActivity main, TimeScaling scale) {
        this.master = main;
        this.timescale = scale;
    }


    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {

    }

    @Override
    public void onSurfaceChanged(GL10 gl, int w, int h) {
        if (fb != null) {
            fb.dispose();
        }
        if (world == null) {
            world = new CubeWorld(w, h, SCALE, generator);
        } else {
            world.changeSurface(w, h);
        }
        for (Observer<CubeWorld> o : observerList) {
            o.update(this, world);
        }
        fb = new FrameBuffer(gl, w, h);


        if (master == null) {
            MemoryHelper.compact();
        }
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        long endTime = SystemClock.elapsedRealtime();
        long elapsedMilliSeconds = endTime - lastTime;
        float elapsedSeconds = elapsedMilliSeconds / 1000f;

        fb.clear();
        world.nextTimeStep(currentScale * lastTime / 1000f, currentScale * endTime / 1000f, currentScale * elapsedSeconds);
        world.renderScene(fb);
        world.draw(fb);
        fb.display();

        if (SystemClock.elapsedRealtime() - time >= 1000) {
            Logger.log(fps + "fps");
            fps = 0;
            time = SystemClock.elapsedRealtime();
        }

        fps++;
        lastTime = endTime;

    }

    @Override
    public void register(Observer<CubeWorld> observer) {
        observerList.add(observer);
    }

    @Override
    public boolean unregister(Observer<CubeWorld> observer) {
        return observerList.remove(observer);
    }

    @Override
    public void prepare(Runnable finished) {
        loadTextures();
        generator.prepare(finished);
    }

    @Override
    public void update(Observable<CubeRiddle> source, CubeRiddle object) {
        lastTime = SystemClock.elapsedRealtime();
        this.currentScale = timescale.getTimeScale();
    }
}
