package com.cubeworld.cubes;


import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cubeworld.ad.AdManager;
import com.cubeworld.func.Interrupting;
import com.cubeworld.func.Observable;
import com.cubeworld.func.Observer;
import com.cubeworld.func.PersistanceManager;
import com.cubeworld.json.JSonAsynchronPoolImpl;
import com.cubeworld.json.JsonAsynchronPool;
import com.cubeworld.main.R;
import com.cubeworld.misc.Persistable;
import com.cubeworld.misc.Preparable;
import com.cubeworld.func.StopResumable;
import com.cubeworld.misc.PreparingPool;
import com.cubeworld.misc.Settings;
import com.cubeworld.modes.SurvivalModus;
import com.cubeworld.rating.Match;
import com.cubeworld.modes.GameMode;
import com.cubeworld.modes.TrainingMode;
import com.cubeworld.modes.RatedModus;
import com.cubeworld.sound.CubeSoundController;
import com.cubeworld.sound.CubeSoundEvent;
import com.cubeworld.sound.CubeSoundPlayer;
import com.cubeworld.ui.AnswerManager;
import com.cubeworld.ui.ProgressBarController;
import com.cubeworld.ui.RatingAdapter;
import com.threed.jpct.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLDisplay;


public class PlayActivity extends AppCompatActivity implements StopResumable, PersistanceManager {

    public static Context CONTEXT;

    private GLSurfaceView mGLView;
    private CubeWorldRenderer renderer = null;
    private CubeController controller;


    private List<StopResumable> stopResumables = new ArrayList<>();
    private List<Persistable> persistables = new ArrayList<>();

    private GameMode gamemode;

    private ExecutorService global_threadpool = Executors.newCachedThreadPool();
    private JsonAsynchronPool gson_pool = new JSonAsynchronPoolImpl(global_threadpool);

    private static PlayActivity instance;

    private ProgressDialog progressDialog;

    @Override
    public void onResumeDo() {
        mGLView.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
    }

    @Override
    public void onStopDo() {
        mGLView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }

    @Override
    public void registerPersistable(Persistable persistable) {
        persistables.add(persistable);
    }

    @Override
    public void unregisterPersistable(Persistable persistable) {
        persistables.remove(persistable);
    }

    public void registerStopResumable(StopResumable... r) {
        for (StopResumable sr : r) {
            stopResumables.add(sr);
        }
    }

    private GameMode parseGamemode() {
        int playModeId = getIntent().getIntExtra("gamemode", TrainingMode.DEFAULT_LOCAL_PLAY_MODE.toInt());
        if (playModeId == RatedModus.PLAYMODE_ID) return gamemode = new RatedModus();
        if (playModeId == SurvivalModus.PLAYMODE_ID) return gamemode = new SurvivalModus();
        return gamemode = TrainingMode.parseFromInt(playModeId);

    }


    public boolean isWifiConnected() {
        ConnectivityManager cm =
                (ConnectivityManager) CONTEXT.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI && cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }

    public static PlayActivity getInstance() {
        if (instance == null) throw new RuntimeException("Play activity has not been yet created");
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        CONTEXT = getApplicationContext();
        instance = this;

        parseGamemode();
        initRenderer();

        controller = new CubeController(gamemode.getRiddleSupplier(), gamemode);

        List<Preparable> preparables = Arrays.asList(renderer, controller, gamemode);
        initProgressDialog();

        final PreparingPool pool = new PreparingPool(preparables, progressDialog, this.global_threadpool);

        pool.prepare(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initEventListeners();
                    }
                });
            }
        });
    }

    private void initProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle(getResources().getString(R.string.loadingTitle));
        progressDialog.setMessage(getResources().getString(R.string.loading_message));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }


    private void initEventListeners() {

        /**
         * First we set up the content view and init gl view and renderer.
         */
        setContentView(R.layout.game);
        RelativeLayout l = (RelativeLayout) findViewById(R.id.cubeLayout);
        l.addView(mGLView);
        mGLView.setRenderer(renderer);

        /**
         * No follows the glue code which connects all the observers to each other.
         * Notice how the following code is only dependent of the gamemode interface,
         * which abstracitfies completely the gamemode.
         */

        //The renderer must be informed about riddle rounds in order to manage the world time events.
        controller.register(renderer);

        //we register interruptings now, which my interrupt the game after a round has been played
        controller.registerInterrupting(AdManager.get());
        for (Interrupting i : gamemode.getInterruptings()){
            controller.registerInterrupting(i);
        }

        /**
         * The answer manager is the observable of a user guess.
         * It must listen to next riddle invocation in order to update the answer buttons.
         * It must be observed by the controller in ordner to spawn a new riddle and inform
         * other listeners about the match result.
         */
        final AnswerManager answers = new AnswerManager(this);
        controller.register(answers);
        answers.register(controller);

        /**
         * Dependet on gamemode we register match observers implementation here.
         * In the case of training game, this will simply trigger MP-elo calcuation.
         * (by chaning players elo, which itsself is a observable<double> once again.
         * In the case of rated game, this will also trigger statistic manager, which uploads
         * player statistics to the backend in order to improve riddle ratings
         * In the case of survival, dependent on the result the scaling to time & difficulty will apply,
         * and the highscore will improve by the cubes counted.
         */

        for (Observer<Match> observer : gamemode.getMatchObservers()) {
            controller.getMatchController().register(observer);
        }

        /**
         * This method adds a listener to the renderer,which will add every newly created as a listener to
         * the game controller, thereby beeing able to 'load' cuberiddle into the cubeworld.
         */
        informControllerAboutNewCubeWorlds();

        /**
         * The rating adapter listens to the change of a players rating, thereby updating the 'rating' in an anmiated way.
         */
        RatingAdapter ad = new RatingAdapter((TextView) findViewById(R.id.elo_textview), gamemode.getPlayer().getRating());
        gamemode.getPlayer().register(ad);

        /**
         * We let the gamemode style a label on the top left, which will carry text information about the current game mode (*maybe icon to come*).
         */

        gamemode.styleLabel((TextView) findViewById(R.id.gamemode));

        /**
         * Finally the progressbar controller will listen to match events
         */

        final ProgressBar progress = (ProgressBar) findViewById(R.id.timeleft);
        controller.register(new ProgressBarController(progress, controller, gamemode.getTimeScaling()));

        /**
         * Check if sound is on, and if so init it.
         */

        if (Settings.get().isSound_effets_on()) {
            initSound(controller);
        }

        /**
         * Every gamemode wants to persist some information. We register those persistables,
         * so in case of a pause, we will save the state than.
         */

        for (Persistable p : gamemode.getPersistables()) {
            this.registerPersistable(p);
        }

        /**
         * A gamemmode might define some conditions when the normal game flow is interrupted.
         */

        for (Interrupting i : gamemode.getInterruptings()) {
            controller.registerInterrupting(i);
        }

        /**
         * In the last step, we register the stop resumables, which will trigger upon pause/resume.
         * As resume will be called after this method on startup, this means
         *
         */

        registerStopResumable(controller, AdManager.get(), this);

        listAccounts();

        Logger.log("Event listeners finished");
    }


    private void listAccounts(){
        AccountManager manager = (AccountManager) getSystemService(ACCOUNT_SERVICE);
        Account[] list = manager.getAccounts();
        for (Account a : list){
            Logger.log("acc" + a.name);
            Logger.log("acc" + a.type);
        }
    }

    public static ExecutorService getGlobalThreadPool() {
        return instance.global_threadpool;
    }

    public static JsonAsynchronPool getJSoonPool() {
        return instance.gson_pool;
    }

    private void initSound(CubeController controller) {
        final CubeSoundController soundController = new CubeSoundController();
        final CubeSoundPlayer player = new CubeSoundPlayer();
        player.prepareSound(CubeSoundEvent.CUBE_DROPPED, R.raw.single_oil_can);
        player.prepareSound(CubeSoundEvent.RIDDLE_SOLVED, R.raw.correct_answer);
        player.prepareSound(CubeSoundEvent.RIDDLE_FAILED, R.raw.wrong_answer);

        soundController.register(player);

        renderer.register(new Observer<CubeWorld>() {
            @Override
            public void update(Observable<CubeWorld> source, CubeWorld object) {
                object.register(soundController.asObserver(CubeWorldEvent.class));
            }
        });
        controller.getMatchController().register(soundController.asObserver(Match.class));
    }


    private void informControllerAboutNewCubeWorlds() {
        renderer.register(new Observer<CubeWorld>() {
            CubeWorld old = null;

            @Override
            public void update(Observable<CubeWorld> source, CubeWorld object) {
                controller.register(object);
                if (old != null) {
                    controller.unregister(old);
                    return;
                } else {
                    controller.start();
                }
                old = object;
            }
        });
    }

    private void initRenderer() {
        mGLView = new GLSurfaceView(getApplication());
        mGLView.setEGLConfigChooser(new GLSurfaceView.EGLConfigChooser() {
            public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display) {
                // Ensure that we get a 16bit framebuffer. Otherwise, we'll fall
                // back to Pixelflinger on some device (read: Samsung I7500)
                int[] attributes = new int[]{EGL10.EGL_DEPTH_SIZE, 16,
                        EGL10.EGL_NONE};
                EGLConfig[] configs = new EGLConfig[1];
                int[] result = new int[1];
                egl.eglChooseConfig(display, attributes, configs, 1, result);
                return configs[0];
            }
        });
        /**
         * The renderer neeeds to have information about the time scale of the game mode.
         */
        renderer = new CubeWorldRenderer(this, gamemode.getTimeScaling());
    }


    @Override
    public void onResume() {
        super.onResume();

        mGLView.onResume();
        for (StopResumable sr : stopResumables) {
            sr.onResumeDo();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mGLView.onPause();
        for (StopResumable sr : stopResumables) {
            sr.onStopDo();
        }
        for (Persistable p : this.persistables) {
            p.persist();
        }
    }
}
