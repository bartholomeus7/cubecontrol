package com.cubeworld.cubes;

public enum CubeWorldEvent {
    CUBE_DROP, ALL_CUBES_REACHED_GROUND
}