package com.cubeworld.cubes;

import com.cubeworld.func.Continuos;
import com.cubeworld.func.Observable;
import com.cubeworld.func.Observer;
import com.cubeworld.main.R;
import com.cubeworld.riddles.CubeRiddle;
import com.cubeworld.riddles.Falling;
import com.threed.jpct.Camera;
import com.threed.jpct.Light;
import com.threed.jpct.Logger;
import com.threed.jpct.Object3D;
import com.threed.jpct.SimpleVector;
import com.threed.jpct.World;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import com.cubeworld.func.Pool;
import com.threed.jpct.util.ExtendedPrimitives;

/**
 * Created by fortknock on 22.12.15.
 */
public class CubeWorld extends World implements Continuos, Observer<CubeRiddle>, Observable<CubeWorldEvent> {


    private List<Observer<CubeWorldEvent>> observers = new ArrayList<>();

    /**
     *
     */


    private Set<Object3D> stableCubes = new HashSet<>();
    private boolean total_stability = false;


    private static final float CUBE_SIZE = 5f;
    private static final float SCALE_VIEWPORT = 1.25f;

    private final Map<Object3D, Falling> fallingCubes = new HashMap<Object3D, Falling>();

    private CubeRiddle currentRiddle;
    private CubeRiddle nRiddle;
    private AtomicBoolean nextRiddle = new AtomicBoolean(false);

    private final SimpleVector SUN_POSITION = SimpleVector.create(0f, -10f * CUBE_SIZE, 0f);
    private final Pool.PreparablePool<Object3D> cubepool;

    private int w;
    private int h;

    private static final float CAM_ROTATION_SPEED_PS = (float) Math.PI / 6;
    private static final float START_ROTATION = (float) 0;

    private SimpleVector dv;
    private SimpleVector camCenter;
    private float currentRotation;
    //   private float currentRedLight=50;


    public CubeWorld(int w, int h, float scale, Pool.PreparablePool<Object3D> cubePool) {
        super();
        this.w = w;
        this.h = h;
        this.cubepool = cubePool;
        initLightening();
        loadBackground();
    }

    private void initLightening() {
        this.setAmbientLight(50, 50, 50);
        Light sun = new Light(this);
        sun.setIntensity(250, 250, 250);
        sun.setPosition(SUN_POSITION);
    }

    private void loadBackground() {
        Object3D background = ExtendedPrimitives.createPlane(10f * CUBE_SIZE, 30);
        background.translate(0, 10f, 0);
        background.calcTextureWrap();
        background.setTexture(PlayActivity.CONTEXT.getResources().getResourceName(R.raw.background));
        background.strip();
        background.build();
        this.addObject(background);
    }

    private void initCamera(CubeRiddle r, float scale) {
        currentRotation = START_ROTATION;
        relocateCamera(r, scale, currentRotation);
    }

    private void relocateCamera(CubeRiddle r, float scale, float currentRotation) {
        Logger.log(r.toString());
        camCenter = new SimpleVector(scale * r.getWidth() / 2f, scale * -(r.getHeight() - 1) / 2f, scale * r.getDepth() / 2f - scale);
        Logger.log(camCenter.toString());
        dv = SimpleVector.create(0f, -1f, -1f).normalize();
        Camera cam = this.getCamera();
        // float xShould = scale * Math.max(r.getWidth(), r.getDepth());
        int xzMax = Math.max(r.getWidth(), r.getDepth());
        float xShould = scale * (float) (Math.sqrt(r.getWidth() * r.getWidth() + r.getDepth() * r.getDepth()));
        float yShould = scale * (float) (Math.sqrt(xzMax * xzMax + r.getHeight() * r.getHeight()));

        float YFOV = (float) (2 * Math.atan(cam.getFOV() / 2 * h / w));
        float XFOV = (float) (2 * Math.atan(cam.getFOV() / 2 * w / h));
        float yTan = (float) Math.tan(YFOV);
        float xTan = (float) Math.tan(XFOV);
        float distance = Math.max(yShould / yTan, xShould / xTan) * SCALE_VIEWPORT;

        dv.scalarMul(distance);
        SimpleVector current = new SimpleVector(dv);
        current.rotateAxis(SimpleVector.create(0, 1, 0), currentRotation);
        current.add(camCenter);
        cam.setPosition(current);
        cam.lookAt(camCenter);
    }

    private void rotateCamera(float dt) {
        currentRotation += dt * CAM_ROTATION_SPEED_PS;
        SimpleVector current = new SimpleVector(dv);
        current.rotateAxis(SimpleVector.create(0, 1, 0), currentRotation);
        current.add(camCenter);
        Camera cam = this.getCamera();
        cam.setPosition(current);
        cam.lookAt(camCenter);
    }

    public CubeRiddle getCurrentRiddle() {
        return currentRiddle;
    }

    @Override
    public void nextTimeStep(float lastTime, float thisTime, float timePassed) {


        if (nextRiddle.getAndSet(false)) {
            loadNextRiddle(5f);
        }

        boolean cube_dropped = false;

        for (Map.Entry<Object3D, Falling> fc : fallingCubes.entrySet()) {
            Object3D cube = fc.getKey();
            float cubeY = cube.getTransformedCenter().y;
            float destY = fc.getValue().stopY;
            float speed = fc.getValue().speedY * timePassed;
            if (cubeY + speed >= destY) {
                cube.translate(0, -(cubeY - destY), 0);
                cube_dropped |= stableCubes.add(cube);
            } else {
                cube.translate(0, speed, 0);
            }
        }

        if (cube_dropped) {
            informObservers(CubeWorldEvent.CUBE_DROP);
        }
        //=if total stability reached first time
        if (total_stability ^ (total_stability = stableCubes.size() == fallingCubes.size())) {
            informObservers(CubeWorldEvent.ALL_CUBES_REACHED_GROUND);
        }


        rotateCamera(timePassed);

    }

    private void informObservers(CubeWorldEvent event) {
        for (Observer<CubeWorldEvent> l : observers) {
            l.update(this, event);
        }
    }

    public void changeSurface(int w, int h) {
        this.w = w;
        this.h = h;
        relocateCamera(currentRiddle, CUBE_SIZE, currentRotation);
    }


    private void clearCubes() {
        for (Object3D cube : fallingCubes.keySet()) {
            cubepool.repool(cube);
            this.removeObject(cube);
        }
        fallingCubes.clear();
    }

    private void setCubes(CubeRiddle riddle, float scale) {
        for (Falling f : riddle.getIntialPosition()) {
            Object3D cube = cubepool.getPooledInstance();
            f = f.scale(scale);
            cube.translate(SimpleVector.create(f.startX + 0.5f * scale, f.startY, f.startZ - 0.5f * scale));
            fallingCubes.put(cube, f);
            this.addObject(cube);
        }
    }

    public void loadNextRiddle(float scale) {
        clearCubes();
        currentRiddle = nRiddle;
        initCamera(currentRiddle, scale);
        setCubes(currentRiddle, CUBE_SIZE);
    }

    @Override
    public void update(Observable<CubeRiddle> source, CubeRiddle object) {
        nRiddle = object;
        resetStatbility();
        nextRiddle.set(true);
    }

    private void resetStatbility() {
        stableCubes = new HashSet<>();
        total_stability = false;
    }

    @Override
    public void register(Observer<CubeWorldEvent> observer) {
        observers.add(observer);
    }

    @Override
    public boolean unregister(Observer<CubeWorldEvent> observer) {
        return observers.remove(observer);
    }
}
