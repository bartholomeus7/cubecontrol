package com.cubeworld.cubes;

import android.os.SystemClock;

import com.cubeworld.func.Interrupable;
import com.cubeworld.func.Interrupting;
import com.cubeworld.func.Observable;
import com.cubeworld.func.Observer;
import com.cubeworld.misc.Preparable;
import com.cubeworld.func.StopResumable;
import com.cubeworld.func.Supplier;
import com.cubeworld.rating.MultipleChoiceResult;
import com.cubeworld.riddles.CubeRiddle;
import com.cubeworld.rating.Match;
import com.cubeworld.modes.GameMode;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;


/**
 * This is the main controller of the play activity. It schedules new riddles via
 */

public class CubeController implements Observable<CubeRiddle>, Observer<Integer>, StopResumable, Preparable, Interrupable {
    private final List<Observer<CubeRiddle>> observers = new ArrayList<>();
    private final List<Interrupting> interruptings = new ArrayList<>();
    private Supplier<CubeRiddle> riddleGenerator;

    private long startTime;

    private CubeRiddle currentRiddle;
    private MatchController matchController = new MatchController();
    private final GameMode mode;


    private AtomicBoolean stopped = new AtomicBoolean(false);

    @Override
    public void onResumeDo() {
        stopped.set(false);
        start();
    }

    @Override
    public void onStopDo() {
        stopped.set(true);
    }

    @Override
    public void prepare(Runnable finished) {
        if (riddleGenerator instanceof Preparable) {
            ((Preparable) riddleGenerator).prepare(finished);
        } else {
            finished.run();
        }
    }

    @Override
    public void registerInterrupting(Interrupting i) {
        interruptings.add(i);
    }

    @Override
    public boolean unRegisterInterrupting(Interrupting i) {
        return false;
    }


    public class MatchController implements Observable<Match> {
        private List<Observer<Match>> observerList = new ArrayList<>();

        @Override
        public void register(Observer<Match> observer) {
            observerList.add(observer);
        }

        @Override
        public boolean unregister(Observer<Match> observer) {
            return observerList.remove(observer);
        }

        public void update(Match m) {
            for (Observer<Match> o : observerList) {
                o.update(this, m);
            }
        }
    }

    public CubeController(Supplier<CubeRiddle> riddleGenerator, GameMode mode) {
        this.riddleGenerator = riddleGenerator;
        this.mode = mode;
    }

    public void setCubeRiddleSupplier(Supplier<CubeRiddle> supplier) {
        this.riddleGenerator = supplier;
    }

    private void generateNewRiddle() {
        currentRiddle = riddleGenerator.get();
        for (Observer<CubeRiddle> o : observers) {
            o.update(this, currentRiddle);
        }
        startTime = SystemClock.elapsedRealtime();
    }

    @Override
    public void register(Observer<CubeRiddle> observer) {
        observers.add(observer);
    }

    @Override
    public boolean unregister(Observer<CubeRiddle> observer) {
        return observers.remove(observer);
    }

    @Override
    public void update(Observable<Integer> source, Integer object) {
        MultipleChoiceResult result = object.equals(currentRiddle.getSolution()) ? MultipleChoiceResult.RIGHT_ANSWER : MultipleChoiceResult.WRONG_ANSWER;
        next(result);
    }

    public MatchController getMatchController() {
        return matchController;
    }

    public void start() {
        generateNewRiddle();
    }

    public void next(MultipleChoiceResult result) {
        float timeSpend = (SystemClock.elapsedRealtime() - startTime) / 1000f;
        matchController.update(new Match(mode.getPlayer(), currentRiddle, result, timeSpend));
        if (stopped.get()) return;
        if (interruptings.isEmpty()) {
            generateNewRiddle();
        } else {
            interruptings.get(0).possibleInterrupt(new Runnable() {
                int index = 1;

                @Override
                public void run() {
                    if (index >= interruptings.size()) {
                        generateNewRiddle();
                    } else {
                        interruptings.get(index++).possibleInterrupt(this);
                    }
                }
            });
        }

    }

}
