package com.cubeworld.cubes;

import com.cubeworld.main.R;
import com.threed.jpct.Loader;
import com.threed.jpct.Logger;
import com.threed.jpct.Object3D;
import com.threed.jpct.util.ExtendedPrimitives;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Stack;

import com.cubeworld.func.Pool;

/**
 * Created by fortknock on 12.01.16.
 */
public class CubePool implements Pool.PreparablePool<Object3D> {

    private static final int[] textureIds = new int[]{R.raw.x212121, R.raw.x455a64, R.raw.xff5722, R.raw.x607d8b, R.raw.xcfd8dc};
    public final static List<String> CUBE_TEXTURES = initTextureNames();

    private final Stack<Object3D> cubePool = new Stack<Object3D>();
    private final float cubeSize;
    private final List<Object3D> motherCubes = new ArrayList<>();
    private final int maxCache;
    private int currentSize = CUBE_TEXTURES.size();

    public CubePool(float cubeSize, int maxCache) {
        this.cubeSize = cubeSize;
        this.maxCache = maxCache;
    }

    public static final int[] getTextureIdsUsed() {
        return Arrays.copyOf(textureIds, textureIds.length);
    }

    private void prepareMotherCubes(float cubeSize) {
        Object3D motherCube = buildMotherCube(cubeSize);
        for (int i = 1; i < CUBE_TEXTURES.size(); i++) {
            Object3D clone = motherCube.cloneObject();
            clone.setTexture(CUBE_TEXTURES.get(i));
            clone.strip();
            clone.build();
            motherCubes.add(clone);
        }
        motherCube.setTexture(CUBE_TEXTURES.get(0));
        motherCube.strip();
        motherCube.build();
        motherCube.compile();
        motherCubes.add(motherCube);
    }

    private static List<String> initTextureNames() {
        List<String> textureNames = new ArrayList<>();
        for (int tid : textureIds) {
            textureNames.add(PlayActivity.CONTEXT.getResources().getResourceName(tid));
        }
        return Collections.unmodifiableList(textureNames);
    }

    @Override
    public void repool(Object3D cube) {
        cube.clearTranslation();
        cubePool.push(cube);
    }

    @Override
    public Object3D getPooledInstance() {
        if (!cubePool.isEmpty()) {
            Collections.shuffle(cubePool);
            return cubePool.pop();
        }
        return buildCube(cubeSize);
    }

    private Object3D buildMotherCube(float size) {
        Object3D cube;
        try {
            cube = Object3D.mergeAll(Loader.loadOBJ(PlayActivity.CONTEXT.getAssets().open("roundedCube.obj"), null, size));
        } catch (IOException e) {
            Logger.log(e);
            cube = ExtendedPrimitives.createCube(size);
            Logger.log("error");
            throw new RuntimeException("Could not  built mother cube");
        }
        return cube;
    }

    private Object3D buildCube(float size) {
        Random r = new Random();
        return motherCubes.get(r.nextInt(CUBE_TEXTURES.size())).cloneObject();
    }

    @Override
    public void prepare(Runnable finished) {
        prepareMotherCubes(cubeSize);
        for (int i = 0; i < maxCache - currentSize; i++) {
            cubePool.push(buildCube(cubeSize));
        }
        finished.run();
    }
}
