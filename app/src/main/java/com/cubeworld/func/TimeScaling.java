package com.cubeworld.func;

/**
 * Created by fortknock on 24.06.16.
 */
public interface TimeScaling {
    float getTimeScale();

    TimeScaling STANDARD_TIMESCALE = new TimeScaling() {
        @Override
        public float getTimeScale() {
            return 1f;
        }
    };
}
