package com.cubeworld.func;

/**
 * Created by fortknock on 30.12.15.
 */
public interface Supplier<X> {
    X get();
}
