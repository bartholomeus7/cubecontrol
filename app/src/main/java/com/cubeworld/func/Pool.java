package com.cubeworld.func;

import com.cubeworld.misc.Preparable;

/**
 * Created by fortknock on 12.01.16.
 */
public interface Pool<X> {
    X getPooledInstance();

    void repool(X instance);

    interface PreparablePool<X> extends Pool<X>, Preparable {

    }
}
