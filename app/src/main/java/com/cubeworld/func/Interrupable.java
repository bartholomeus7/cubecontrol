package com.cubeworld.func;

/**
 * Created by fortknock on 27.06.16.
 */
public interface Interrupable {

    void registerInterrupting(Interrupting i);

    boolean unRegisterInterrupting(Interrupting i);


}
