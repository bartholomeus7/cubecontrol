package com.cubeworld.func;

/**
 * Created by fortknock on 16.01.16.
 */
public interface Observable<X> {
    void register(Observer<X> observer);

    boolean unregister(Observer<X> observer);
}
