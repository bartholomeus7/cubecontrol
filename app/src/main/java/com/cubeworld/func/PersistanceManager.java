package com.cubeworld.func;

import com.cubeworld.misc.Persistable;

/**
 * Created by fortknock on 24.06.16.
 */
public interface PersistanceManager {

    void registerPersistable(Persistable persistable);

    void unregisterPersistable(Persistable persistable);
}
