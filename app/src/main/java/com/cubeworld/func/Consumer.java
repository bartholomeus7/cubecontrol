package com.cubeworld.func;

/**
 * Created by fortknock on 10.01.16.
 */
public interface Consumer<X> {
    void consume(X x);
}
