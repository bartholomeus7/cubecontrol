package com.cubeworld.func;

/**
 * Created by fortknock on 25.01.16.
 */
public interface Interrupting {
    boolean possibleInterrupt(Runnable onInterruptFinish);
}
