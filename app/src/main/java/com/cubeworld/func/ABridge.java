package com.cubeworld.func;

/**
 * Created by fortknock on 16.01.16.
 */
public interface ABridge<X> extends AConsumer<Object>, ASupplier<X> {
}
