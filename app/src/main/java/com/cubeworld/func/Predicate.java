package com.cubeworld.func;

/**
 * Created by fortknock on 26.12.15.
 */
public interface Predicate<G> {
    boolean accept(G g);


}
