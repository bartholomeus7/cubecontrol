package com.cubeworld.func;

import com.threed.jpct.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Following the old java naming conventions, this class include some static methods useful for funcitonals.
 */

public class Functionals {

    private Functionals() {
        throw new RuntimeException("Static utility class");
    }

    public static <G> Predicate<G> and(final Predicate<G> first, final Predicate<G> second) {
        return new Predicate<G>() {
            @Override
            public boolean accept(G g) {
                return first.accept(g) && second.accept(g);
            }
        };
    }

    public static final Predicate<?> TRUE = new Predicate<Object>() {
        @Override
        public boolean accept(Object o) {
            return true;
        }
    };

    public static final Predicate<?> FALSE = new Predicate<Object>() {
        @Override
        public boolean accept(Object o) {
            return false;
        }
    };

    public static <X> List<X> mapEach(Collection<X> collection, GMap<X> map) {
        List<X> result = new ArrayList<X>();
        for (X x : collection) {
            result.add(map.map(x));
        }
        return result;
    }

    public static <X> List<X> filter(Collection<X> collection, Predicate<X> predicate) {
        List<X> result = new ArrayList<X>();
        for (X x : collection) {
            if (predicate.accept(x)) result.add(x);
        }
        return result;
    }

    public static <X, Y> Map<Y, X> reverseMap(Map<X, Y> map) {
        Map<Y, X> result = new HashMap<>();
        for (Map.Entry<X, Y> e : map.entrySet()) {
            result.put(e.getValue(), e.getKey());
        }
        return result;
    }

    public static class NoElementSuppliableexception extends Exception {
        public NoElementSuppliableexception(String msg) {
            super(msg);
        }
    }

    public static <X> Iterable<List<X>> getConnectedSubsetsList(final List<X> connected) {
        return new Iterable<List<X>>() {
            @Override
            public Iterator<List<X>> iterator() {
                return new Iterator<List<X>>() {
                    int i = -1;
                    int k = 1;
                    int total = 0;

                    @Override
                    public boolean hasNext() {
                        return total < 2 * connected.size();
                    }

                    @Override
                    public List<X> next() {
                        Logger.log(String.valueOf(connected.size()));
                        if (i + k <= connected.size()) {
                            i++;
                        } else {
                            k++;
                            i = 0;
                        }
                        total++;
                        return connected.subList(i, i + k);
                    }

                    @Override
                    public void remove() {
                        throw new UnsupportedOperationException("Makes no sense");
                    }
                };
            }

        };
    }
}
