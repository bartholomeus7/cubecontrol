package com.cubeworld.func;

/**
 * Created by fortknock on 24.01.16.
 */
public interface StopResumable {
    void onResumeDo();

    void onStopDo();
}
