package com.cubeworld.func;

/**
 * Created by fortknock on 16.01.16.
 */
public interface Observer<X> {
    void update(Observable<X> source, X object);
}
