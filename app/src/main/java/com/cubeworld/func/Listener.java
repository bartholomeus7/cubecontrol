package com.cubeworld.func;

/**
 * Created by fortknock on 10.01.16.
 */
public interface Listener<X, Y> {
    void action(X x, Y y);
}
