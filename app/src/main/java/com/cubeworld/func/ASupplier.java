package com.cubeworld.func;

/**
 * Asynchronous supplier.
 * Created by fortknock on 12.01.16.
 */
public interface ASupplier<X> {
    void call(Listener<X, Throwable> callback);
}
