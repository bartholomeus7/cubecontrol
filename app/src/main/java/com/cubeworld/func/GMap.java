package com.cubeworld.func;

/**
 * Created by fortknock on 03.01.16.
 */
public interface GMap<X> {
    X map(X x);

}
