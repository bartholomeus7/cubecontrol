package com.cubeworld.func;

/**
 * Created by fortknock on 16.01.16.
 */
public interface AConsumer<X> {
    void consume(X x, Listener<X, Throwable> callback);
}
