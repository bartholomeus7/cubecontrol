# Cube Control #

![ic_launcher.png](https://bitbucket.org/repo/B7Ao78/images/2781473478-ic_launcher.png)

*Yes, its SEVEN!*

Cube Control is a small android game which challenges its players to solve riddles in a certain time span. Having currently three different game modes, the players task is simple: Count the 3D-Cubes falling from above!

## Game Modes ##

Currently, the game supports three different gamemodes: 

* A *rated gamemode*, in which players can establish a rating based on an ELO-like rating system in which each riddle is assigned to a difficulty, and in which the challenge increases with the players capability.
* A *training mode*, in which the players can choose the difficulty they feel ready to face at the moment.
* A *survival mode*, in which both difficulty and time pressure increase gradually, leading to the  
unevitably defeat of the player, who in return and with his last breath, might submit a highscore.



## Technical Details ##

Being developed in Android Studio, this Project uses Gradle as Build Management Tool, JCPT-AE as 3D engine and a google cloud application (java EE) as backend . In the future, a *live modus* is planned, which will challenge players to solve riddles quicker than their opponents.
This mode is already implemented PoC-like in javascript and on the backend side using (and abusing) the Google Channel API . 


![device-2016-06-28-202700.png](https://bitbucket.org/repo/B7Ao78/images/4288140416-device-2016-06-28-202700.png)


*Screenshot*